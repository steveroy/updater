/*
 * Copyright 2008 Steve Roy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.java.updater;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

/**
 *
 * @author Steve Roy
 */
class UpdateFrame extends JFrame {

	/**
	 * The parent updater.
	 */
	private Updater updater;
	
	/**
	 * The channel item holding the available update.
	 */
	private ChannelItem update;
	
	/**
	 * The Remind Later button.
	 */
	private JButton remindButton;
	
	/**
	 * Construct an update frame.
	 * @param updater the parent updater
	 * @param applicationName the name of the application
	 * @param applicationIcon the icon of the application
	 * @param currentVersion the current version of the application
	 * @param update the channel item holding the available update
	 */
	public UpdateFrame(Updater updater, String applicationName,
		Icon applicationIcon, Version currentVersion, ChannelItem update) {
		super();
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					doRemind();
				}
			});
        
		this.updater = updater;
		this.update = update;

		ResourceBundle i18n =
			ResourceBundle.getBundle("net.java.updater.Strings");

		setTitle(i18n.getString("UpdateFrame.title"));

		JPanel c = (JPanel) getContentPane();
		c.setBorder(BorderFactory.createEmptyBorder(18, 18, 18, 18));
		c.setLayout(new BorderLayout(20, 0));

		// Icon panel
		JPanel left = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		left.add(new JLabel(applicationIcon));
		c.add(left, BorderLayout.WEST);

		// Center panel
		JPanel center = new JPanel(new BorderLayout(0, 8));
		c.add(center, BorderLayout.CENTER);

		// Message panel
		JPanel p = new JPanel(new BorderLayout(0, 8));
		center.add(p, BorderLayout.NORTH);

		JLabel l = new JLabel(MessageFormat.format(
			i18n.getString("UpdateFrame.message"),
			applicationName));
        l.setFont(l.getFont().deriveFont(Font.BOLD));
        p.add(l, BorderLayout.NORTH);
        
        JTextArea ta = new JTextArea(MessageFormat.format(
            i18n.getString("UpdateFrame.question"),
			applicationName, update.getVersion().toString(), currentVersion.toString()));
        ta.setOpaque(false);
        ta.setHighlighter(null);
        ta.setEditable(false);
        ta.setLineWrap(true);
        ta.setWrapStyleWord(true);
		ta.setFocusable(false);
        ta.setFont(UIManager.getFont("Label.font").deriveFont(11.0f));
        p.add(ta, BorderLayout.CENTER);
        
        // Release notes panel
        p = new JPanel(new BorderLayout(0, 5));
        center.add(p, BorderLayout.CENTER);
        
        l = new JLabel(i18n.getString("UpdateFrame.releaseNotesTitle"));
        l.setFont(l.getFont().deriveFont(Font.BOLD, 11));
        p.add(l, BorderLayout.NORTH);
        
        JTextPane tp = new JTextPane();
        tp.setEditable(false);
		tp.setMargin(new Insets(1, 4, 1, 4));
		tp.setBackground(Color.WHITE);
        JScrollPane sp = new JScrollPane(tp);
        sp.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        p.add(sp, BorderLayout.CENTER);
        
		String desc = update.getDescription();
		URL url = null;
		try {
			url = new URL(desc);
		} catch (MalformedURLException ex) {}
		if (url != null) {
			try {
				tp.setPage(url);
			} catch (IOException ex) {
				tp.setText("unable to read");
			}
		} else {
			tp.setText(desc);
		}
		
        // Button panel
        p = new JPanel(new GridBagLayout());
        center.add(p, BorderLayout.SOUTH);
        
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridy = 0;
        
        gbc.weightx = 1;
        gbc.insets.left = 0;
        JButton b = new JButton(i18n.getString("UpdateFrame.skipButton"));
		b.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doSkip();
				}
			});
        p.add(b, gbc);
        
        gbc.weightx = 0;
        gbc.insets.left = 6;
        remindButton = new JButton(i18n.getString("UpdateFrame.remindButton"));
		remindButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doRemind();
				}
			});
        p.add(remindButton, gbc);
        
        gbc.weightx = 0;
        gbc.insets.left = 6;
        b = new JButton(i18n.getString("UpdateFrame.installButton"));
		b.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					doInstall();
				}
			});
        p.add(b, gbc);
        getRootPane().setDefaultButton(b);
        
		final String escapeWindow = "escape-window";
        c.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
			KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), escapeWindow);
        c.getActionMap().put(escapeWindow, new AbstractAction(escapeWindow) {
				public void actionPerformed(ActionEvent e) {
					remindButton.doClick();
				}
			});
		
        setSize(575, 400);
        
		Dimension ss = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension s = getSize();
	    setLocation((ss.width - s.width) / 2, (ss.height - s.height) / 2);
    }

	/**
	 * <p>Close the window and set the ignored version to the current
	 * verion.</p>
	 */
	private void doSkip() {
		setVisible(false);
		dispose();
		
		updater.setIgnoredVersion(update.getVersion());
	}

	/**
	 * <p>Close the window and unset the ignored version.</p>
	 */
	private void doRemind() {
		setVisible(false);
		dispose();
		
		updater.setIgnoredVersion(null);
	}

	/**
	 * <p>Close the window and start the update installing.</p>
	 */
	private void doInstall() {
		setVisible(false);
		dispose();
		
		updater.setIgnoredVersion(null);
		updater.installUpdate(update);
	}
}
