/*
 * Copyright 2008 Steve Roy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.java.updater;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.tools.zip.UnixStat;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;

/**
 * <p>An expansion task extracts a given update file on disk.</p>
 *
 * @author Steve Roy
 * @author Graham Wagener
 */
class ExpansionTask extends Task {

	/**
	 * <p>The location on disk of the update file to expand.</p>
	 */
	private File updateFile;

	/**
	 * <p>The ZipFile object of the update file to expand.</p>
	 */
	private ZipFile zipFile = null;

	/**
	 * <p>The directory the update is expanded into.</p>
	 */
	private File folder;

	/**
	 * <p>A mapping of Unix permissions to files.</p>
	 */
	private Map permissionsMap = new HashMap();

	/**
	 * <p>Construct an expansion task.</p>
	 *
	 * @param updateFile the update file to expand
	 */
	public ExpansionTask(File updateFile) {
		this.updateFile = updateFile;
	}
	
	/**
	 * <p>Execute the task's activity.</p>
	 *
	 * @return <code>true</code> if the task completed successfully
	 * @throws IOException if an error occurs while executing the task
	 */
	public boolean execute() throws IOException {
		changeMessageKey("ExpansionTask.message");
		changeProgressTextKey(null);

		try {
			// Expand the archive
			String ext = parseFileName(updateFile)[1];
			if (ext.toLowerCase().equals("zip")) {
				expandZipFile(updateFile);
			} else {
				throw new IOException("Unsupported file type: " + ext);
			}
		} finally {
			// Delete the archive because we don't need it anymore
			updateFile.delete();
		}

		if (isCanceled()) {
			return false;
		}
		return true;
	}

	/**
	 * <p>Returns an array containing the <code>chmod</code> command suitable
	 * for passing to {@link Runtime.exec(String[])}.  The array contains the
	 * <code>chmod</code> command, the specified mode, and the files to be set
	 * with that mode.</p>
	 *
	 * @param mode a <code>String</code> representation of octal number value
	 *             Unix permissions (no leading zero)
	 * @return an array containing the <code>chmod</code> command and its
	 *         arguments
	 */
	private String[] constructSetPermissionCommand(String mode) {
		List commandList = new ArrayList();
		commandList.add("chmod");
		commandList.add(mode);
		commandList.addAll((Collection) permissionsMap.get(mode));
		return (String[]) commandList.toArray(new String[] {});
	}

	// TODO: Note that it sets folder
	/**
	 * <p>Create a directory in the default temporary directory.</p>
	 *
	 * <p>This method is the directory equivalent of
	 * <code>File.createTempFile()</code>.</p>
	 *
	 * @param prefix the prefix used in generating the directory name
	 * @throws IOException if the directory could not be created
	 */
	private void createTempDirectory(String prefix) throws IOException {
		if (prefix == null) {
			throw new NullPointerException("Prefix is null");
		}
		String tmp = System.getProperty("java.io.tmpdir");
		int index = 1;
		File fo = new File(tmp, prefix);
		while (fo.exists()) {
			index++;
			fo = new File(tmp, prefix + String.valueOf(index));
		}
		if (!fo.mkdirs()) {
			throw new IOException("Directory could not be created");
		}
		folder = fo;
	}

	/**
	 * <p>Expand a zip entry of a directory.</p>
	 *
	 * @param entry the zip entry to expand
	 * @throws IOException if an I/O error occurs
	 */
	private void expandDirectoryEntry(ZipEntry entry) throws IOException {
		createDirectoryHierarchy(new File(folder, entry.getName()));
		markPermissions(entry);
	}

	/**
	 * <p>Expand a zip entry of a file.</p>
	 *
	 * @param entry the zip entry to expand
	 * @throws IOException if an I/O error occurs
	 */
	private void expandFileEntry(ZipEntry entry) throws IOException {
		File f = new File(folder, entry.getName());
		createDirectoryHierarchyForFile(f);
		InputStream inputStream = zipFile.getInputStream(entry);
		OutputStream out = null;
		try {
			out = new BufferedOutputStream(new FileOutputStream(f));
			byte [] buffer = new byte[1024];
			int bytesRead;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				out.write(buffer, 0, bytesRead);
				if (isCanceled()) {
					return;
				}
			}
			markPermissions(entry);
		} finally {
			inputStream.close();
			if (out != null) {
				try {
					out.close();
				} catch (IOException ex) {}
			}
		}
	}

	/**
	 * <p>Expand a zip entry of a symbolic link.</p>
	 *
	 * @param entry the zip entry to expand
	 * @throws IOException if an I/O error occurs
	 */
	// TODO: Check how hard links are handled.
	private void expandLinkEntry(ZipEntry entry) throws IOException {
		File f = new File(folder, entry.getName());
		createDirectoryHierarchyForFile(f);
		InputStream inputStream = zipFile.getInputStream(entry);
		// NOTE: Is there a readLine I can use since this will just have one
		// line?
		StringBuffer contents = new StringBuffer();
		// The contents will only be a file name, so probably pretty short
		byte [] buffer = new byte[32];
		int bytesRead;
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			contents.append(new String(buffer, 0, bytesRead));
			if (isCanceled()) {
				return;
			}
		}
		inputStream.close();

		// NOTE: If we are extracting into a current installation we may need to
		// use the -f flag, but creating a fresh copy this shouldn't be
		// required.  Furthermore, once we implement rollback we'll be
		// sandboxing so it will be the same as a fresh copy.
		String[] command = new String[] {"ln", "-s", contents.toString(),
			f.getAbsolutePath()};
		Runtime.getRuntime().exec(command);

		// Unix permissions are irrelevant for links, so we don't set them.
	}

	/**
	 * <p>Expand a given zip entry.</p>
	 *
	 * @param entry the zip entry to expand
	 * @throws IOException if an I/O error occurs
	 */
	private void expandZipEntry(ZipEntry entry)
		throws IOException {
		if (entry.isDirectory()) {
			expandDirectoryEntry(entry);
		} else if (Platform.getCurrentPlatform().isUnix()
		        && ((entry.getUnixMode() - UnixStat.LINK_FLAG) >> 12) == 0) {
			expandLinkEntry(entry);
		} else {
			expandFileEntry(entry);
		}
	}

	/**
	 * <p>Expand a given zip file into the temporary location.</p>
	 *
	 * @param file the zip file to expand
	 * @throws IOException if an I/O error occurs
	 */
	private void expandZipFile(File file) throws IOException {
		createTempDirectory(parseFileName(file)[0]);

		try {
			// NOTE: Some zip programs tell you the number of bytes processed.
			int numEntriesExpanded = 0;
			// TODO: Check if the stream location is reset at some point, either
			// by calling getEntries or getInputStream(entry), because if it is
			// we can just use zipFile (once it's set) instead of creating a new
			// ZipFile in the method call for getTotalNumberOfEntries.
			int numEntriesTotal = getTotalNumberOfEntries(new ZipFile(file));
			changeProgressValue(numEntriesExpanded);
			changeProgressLength(numEntriesTotal);

			zipFile = new ZipFile(file);
			Enumeration e = zipFile.getEntries();
			ZipEntry entry;
			while (e.hasMoreElements()) {
				// This is first so it will be checked after continue commands.
				if (isCanceled()) {
					return;
				}

				entry = (ZipEntry) e.nextElement();
				if (isIgnoredEntry(entry)) {
					continue;
				}

				expandZipEntry(entry);
				numEntriesExpanded++;
				changeProgressValue(numEntriesExpanded);
			}
			setPermissions();
		} finally {
			if (zipFile != null) {
				try {
					zipFile.close();
				} catch (IOException ex) {}
			}
			if (isCanceled()) {
				// NOTE: Maybe make a deleteFolder() method that calls dRec with
				// folder and sets folder to null.
				deleteRecursively(folder);
			}
		}
	}

	/**
	 * <p>Get the set of files (potentially empty) that are marked with the
	 * given permission string.</p>
	 *
	 * @param mode a <code>String</code> representation of octal number value
	 *             Unix permissions (no leading zero)
	 * @return the <code>Set</code> of files marked with the given permissions
	 */
	private Set getFileSetForMode(String mode) {
		if (permissionsMap.containsKey(mode)) {
			return (Set) permissionsMap.get(mode);
		} else {
			Set files = new HashSet();
			permissionsMap.put(mode, files);
			return files;
		}
	}

	// NOTE: Uses getPlatform and getUnixMode of ZipEntry.  Can I assume that if
	// getUnixMode returns anything (except maybe 0), that platform is Unix?  If
	// platform is Unix then getUnixMode should never return 0 because an entry
	// will always be a file (010000), link (0120000), or dir (040000).
	// TODO: Find out what assumptions I can make.
	// NOTE: The Javadoc isn't quite accurate in the "checking" for Unix
	// permissions
	/**
	 * <p>If running on a Unix system, check the given {@link ZipEntry} for Unix
	 * permissions and, if any exist, mark the expanded file with the
	 * permissions to be set.</p>
	 *
	 * @param entry the <code>ZipEntry</code> to check for permissions
	 */
	private void markPermissions(ZipEntry entry) {
		if (Platform.getCurrentPlatform().isUnix()
		 && entry.getPlatform() == 3) {
			String mode = Integer.toOctalString(entry.getUnixMode());
			mode = mode.substring(mode.length() - 4, mode.length());
			Set files = getFileSetForMode(mode);
			File f = new File(folder, entry.getName());
			files.add(f.getAbsolutePath());
		}
	}

	/**
	 * <p>If running on a Unix system set the permissions of all files that have
	 * been marked with permissions.</p>
	 *
	 * @throws IOException if an I/O error occurs
	 */
	// TODO: Audit methods that throw exceptions and see if some exceptions
	// could be handled further down.
	// NOTE: Since the system has already been checked in markPermissions the
	// permissionsMap should be empty if not Unix.  Therefore, the platform
	// probably doesn't need to be checked.
	private void setPermissions() throws IOException {
		if (Platform.getCurrentPlatform().isUnix()) {
			Runtime r = Runtime.getRuntime();
			Iterator i = permissionsMap.keySet().iterator();
			while (i.hasNext()) {
				String mode = (String) i.next();
				String[] cmdarray = constructSetPermissionCommand(mode);
				r.exec(cmdarray);
			}
		}
	}

	/**
	 * <p>Guarantee that a directory hierarchy exists by creating it if it
	 * doesn't.</p>
	 *
	 * @param fo the last element in the hierarchy
	 * @throws IOException if the hierarchy does not exist and could not be
	 *                     created
	 */
	private static void createDirectoryHierarchy(File fo) throws IOException {
		if (!fo.exists()) {
			boolean created = fo.mkdirs();
			if (!created) {
				throw new IOException("Unable to create directory " + fo);
			}
		}
	}

	/**
	 * <p>Guarantee that all the directory hierarchy below a file exists by
	 * creating it if it doesn't.</p>
	 *
	 * @param f the file at the end of the hierarchy
	 * @throws IOException if the hierarchy does not exist and could not be
	 *                     created
	 */
	private static void createDirectoryHierarchyForFile(File f)
		throws IOException {
		File fo = f.getParentFile();
		createDirectoryHierarchy(fo);
	}

	/**
	 * <p>Utility method to delete a directory including recursively deleting
	 * its contents.</p>
	 *
	 * @param file the file or directory to delete
	 * @return <code>true</code> if the file was deleted successfully
	 */
	private static boolean deleteRecursively(File file) {
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (!deleteRecursively(files[i])) {
					return false;
				}
			}
		}
		return file.delete();
	}

	/**
	 * <p>Get the total number of entries in a provided zip file.</p>
	 *
	 * @param zf the zip file whose total number of entries is returned
	 * @return the total number of entries in the provided zip file
	 */
	private static int getTotalNumberOfEntries(ZipFile zf) {
		int total = 0;
		ZipEntry entry;
		Enumeration e = zf.getEntries();
		while (e.hasMoreElements()) {
			entry = (ZipEntry) e.nextElement();
			if (!isIgnoredEntry(entry)) {
				total++;
			}
		}
		return total;
	}

	/**
	 * <p>Get whether a specifed entry in a zip file should ignored.</p>
	 *
	 * <p>Some files are meaningless when extracted from an zip file.  The
	 * entries for these files should be ignored.  For example, Mac OS resourse
	 * fork aware archives create a __MACOSX directory whose contents are
	 * useless on any other platform.</p>
	 *
	 * @param entry the <code>ZipEntry</code> to check
	 * @return <code>true</code> if the specified entry should be ignored
	 */
	private static boolean isIgnoredEntry(ZipEntry entry) {
		// Ignore __MACOSX directories in zip archives created by a Mac OS
		// resource fork aware archiver
		// NOTE: The resource forks in __MACOSX could actually be necessary for
		// items in an archive created on and intended for a Mac.  For example,
		// all the information in an alias is stored in the resource fork.  If
		// we do want to handle it there are a couple of methods that could
		// possibly work.  Since we're on a Mac if we care about this we can
		// (and will have to) use Mac specific utilities.  The easiest option
		// would be to re-extract the archive with ditto, which will reattach
		// the resource forks to the files during extraction.  See
		// http://www.macosxhints.com/article.php?story=20031118111144197 near
		// the bottom for syntax.  The other option if we really didn't want to
		// re-extract the archive would possibly be to use Rez to manually
		// reattach the resource forks.  This would be very fiddly.
		return entry.getName().startsWith("__MACOSX");
	}

	/**
	 * <p>Parse the full name of a given file into its name and extension.</p>
	 *
	 * <p>The array returned by this method always has a length of two.  If the
	 * file does not have an extension, the second string in the array will be
	 * null.</p>
	 *
	 * @param file the file whose name we want to parse
	 * @return an array of two strings for the name and extension
	 */
	private static String[] parseFileName(File file) {
		String ext = null;
		String name = file.getName();
		int pos = name.lastIndexOf('.');
		if (pos != -1) {
			ext = name.substring(pos + 1);
			name = name.substring(0, pos);
		}
		return new String[] {name, ext};
	}

	/**
	 * <p>Calling an <code>ExpansionTask</code>'s <code>action()</code> method
	 * will cancel the task.</p>
	 */
	public void action() {
		cancel();
	}

	// TODO: This just uses the Javadoc of Task, but it should be specific to
	// this class and tell what it will return.  For instance, this should say
	// that the action name that it returns should be the key to localized
	// version of "cancel".  Once the Javadoc is correct proper tests can be
	// written.  This applies for other Task subclasses as well.
	/**
	 * <p>Return the property list key to the descriptive name of the task's
	 * action.</p>
	 *
	 * @return the property list key to the descriptive name of the task's
	 *         action
	 */
	public String getActionNameKey() {
		return "ExpansionTask.actionName";
	}

	/**
	 * <p>The action for an <code>ExpansionTask</code> should not be the default
	 * action to perform.</p>
	 *
	 * @return <code>false</code>
	 */
	public boolean isActionDefault() {
		return false;
	}

	// NOTE: Currently this is only set after execute() is called.
	// NOTE: folder should possibly only be set if the task doesn't throw an
	// exception and isn't cancelled.  However, getFolder() is never called in
	// either of those cases, so it doesn't matter.
	/**
	 * <p>Get the folder where the update file expanded its contents.</p>
	 *
	 * @return the folder where the update was expanded
	 */
	public File getFolder() {
		return folder;
	}
}
