/*
 * Copyright 2008 Steve Roy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.java.updater;

import java.util.EventListener;

/**
 * An update listener gets notified of events occurring in an {@link Updater}.
 *
 * @author Steve Roy
 */
public interface UpdateListener extends EventListener {

	/**
	 * A check for update failed with an error. The exception that caused the
	 * failure can be retrieved from the event.
	 * @param e the update event
	 */
    void checkFailed(UpdateEvent e);
    
	/**
	 * A check could not find any updates, neither old or new. This indicates
	 * that the appcast is essentially empty.
	 * @param e the update event
	 */
    void updateNotFound(UpdateEvent e);
    
	/**
	 * A check could not find any update newer than the current version. The
	 * version of the current update can be retrieved from the event.
	 * @param e the update event
	 */
	void upToDate(UpdateEvent e);
	
	/**
	 * A check found the update that is currently being ignored. The version of
	 * the ignored update can be retrieved from the event.
	 * @param e the update event
	 */
	void updateIgnored(UpdateEvent e);
	
	/**
	 * A check found an available update. The version of the found update can be
	 * retrieved from the event.
	 * @param e the update event
	 */
	void updateAvailable(UpdateEvent e);
	
	/**
	 * An updater is restarting the application.
	 * @param e the update event
	 */
	void applicationRestarting(UpdateEvent e);
}
