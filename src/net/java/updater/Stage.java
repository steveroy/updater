/*
 * Copyright 2009 Graham Wagener
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.java.updater;

import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * <p>A <code>Stage</code> is an immutable representation of a stage in the
 * software release life cycle.  Stages are ordered by the completeness of the
 * software.  {@linkplain #DEVELOPMENT Development} is the earliest stage,
 * followed by {@linkplain #ALPHA alpha}, then {@linkplain #BETA beta}, to the
 * {@linkplain #FINAL final} stage.  Each stage corresponds to one of more
 * string representations or codes which are used to construct
 * <code>Stage</code> objects.</p>
 *
 * @author Graham Wagener
 */
// There are a lot of other stages that we could support listed at
// http://en.wikipedia.org/wiki/Software_release_life_cycle.  However, it would
// be hard to know what letter codes will be used for parsing.
// NOTE: Consider renaming to VersionStage?
public class Stage implements Comparable {

	/**
	 * <p>The internal representation of the {@link #FINAL}
	 * <code>Stage</code>.</p>
	 */
	private static final String FINAL_CODE = "FINAL";

	/**
	 * <p>This mapping contains all the valid string representations of
	 * <code>Stage</code>s mapped to the ordering associated with each.</p>
	 */
	private static final Map CODES;

	/**
	 * <p>The stage of versions released for internal testing.  Represented as
	 * "a".</p>
	 */
	public static final Stage ALPHA;

	/**
	 * <p>The stage of versions released for external testing.  Represented as
	 * "b".</p>
	 */
	public static final Stage BETA;

	/**
	 * <p>The stage of feature incomplete versions prior to test releases.
	 * Other names for this stage include pre-alpha and nightly.  Represented as
	 * "d".</p>
	 */
	public static final Stage DEVELOPMENT;

	/**
	 * <p>The stage of fully released versions.  There is no representation for
	 * this stage as it is the default.</p>
	 */
	public static final Stage FINAL;

	/**
	 * <p>The penultimate stage of versions prior to final release.  Release
	 * canditates are generally code complete.  Represented as "rc".</p>
	 */
	public static final Stage RELEASE_CANDIDATE;

	static {
		FINAL = new Stage();

		// NOTE: The only use of the sorted map here is so that the codes get
		// printed in order in the exception thrown by the constructor.
		// TODO: Find out if there is a speed difference to the contains search
		// between sorted and hashed.
		Map codes = new TreeMap(new Comparator() {
			public int compare(Object arg0, Object arg1) {
				return ((String) arg0).compareToIgnoreCase((String) arg1);
			}
		});
		// NOTE: Could read this in from a file, properties file maybe.
		codes.put("d", new Integer(0));
		// Oh, for Java 5 autoboxing
		codes.put("a", new Integer(((Integer) codes.get("d")).intValue() + 1));
		codes.put("b", new Integer(((Integer) codes.get("a")).intValue() + 1));
		codes.put("rc", new Integer(((Integer) codes.get("b")).intValue() + 1));
		CODES = Collections.unmodifiableMap(codes);

		// Instantiating these constants before the codes have been added causes
		// errors
		// NOTE: Consider splitting out into an init method
		ALPHA = new Stage("a");
		BETA = new Stage("b");
		DEVELOPMENT = new Stage("d");
		RELEASE_CANDIDATE = new Stage("rc");
	}

	/**
	 * <p>The string representation of this stage.</p>
	 */
	private String code;

	/**
	 * <p>This is a special constructor for the {@link #FINAL}
	 * <code>Stage</code> constant.</p>
	 *
	 * <p><code>FINAL</code> is different than the other stages.  By not
	 * including the code for it in <code>codes</code> it cannot be constructed
	 * publicly.  The only reason for constructing <code>Stage</code>s besides
	 * the constants is for parsing version strings.  Because <code>FINAL</code>
	 * has no representation it can't be parsed and therefore there is no need
	 * for it to be publicly constructible.</p>
	 */
	private Stage() {
		code = FINAL_CODE;
	}

	/**
	 * <p>Construct a <code>Stage</code> from a string representation.  The
	 * purpose of this constructor is for parsing version strings.  Otherwise,
	 * use the constant <code>Stage</code>s.</p>
	 *
	 * <p>Valid representations are listed in the documentation of the constant
	 * <code>Stage</code>s.  <code>null</code> is not a valid
	 * representation.</p>
	 *
	 * @param code the string representation to construct the <code>Stage</code>
	 *             from; may not be <code>null</code>
	 * @throws IllegalArgumentException if <code>code</code> is not valid
	 */
	public Stage(String code) {
		if (code != null && CODES.containsKey(code)) {
			this.code = code;
		} else {
			throw new IllegalArgumentException("code must be one of "
				+ CODES.keySet());
		}
	}

	/**
	 * <p>When at least one of this and the given <code>Stage</code> are
	 * {@link #FINAL} this method compares them for order.  Two
	 * <code>Stage</code>s are equal if they are both final.  Otherwise,
	 * <code>FINAL</code> is greater than a non-final <code>Stage</code>.</p>
	 *
	 * @param other the <code>Stage</code> with which to compare
	 * @return a negative integer, zero, or a positive integer if this
	 *         <code>Stage</code> is respectively less than, equal to, or
	 *         greater than the given <code>Stage</code>
	 */
	private int compareFinalStages(Stage other) {
		if (code.equals(FINAL_CODE) && other.code.equals(FINAL_CODE)) {
			return 0;
		} else if (code.equals(FINAL_CODE)) {
			return 1;
		} else { // other.code.equals(FINAL_CODE)
			return -1;
		}
	}

	/**
	 * <p>Compares this <code>Stage</code> for order with the specified object,
	 * which must be a <code>Stage</code>.  Returns a negative integer, zero, or
	 * a positive integer if this <code>Stage</code> is respectively less than,
	 * equal to, or greater than the specified object.</p>
	 *
	 * <p>The order of stages from lowest to highest is {@link #DEVELOPMENT},
	 * {@link #ALPHA}, {@link #BETA}, {@link #FINAL}.  Any stage is greater than
	 * <code>null</code> so a positive integer will always be returned if the
	 * specified object is <code>null</code>.</p>
	 *
	 * @param stage the <code>Object</code> with which to compare
	 * @return a negative integer, zero, or a positive integer if this
	 *         <code>Stage</code> is respectively less than, equal to, or
	 *         greater than <code>stage</code>
	 * @throws ClassCastException if the specified object is not a
	 *                            <code>Stage</code> or subclass of
	 *                            <code>Stage</code>
	 */
	public int compareTo(Object stage) {
		if (stage == null) {
			return 1;
		}
		Stage other = (Stage) stage;
		if (code.equals(FINAL_CODE) || other.code.equals(FINAL_CODE)) {
			return compareFinalStages(other);
		} else {
			Integer thisOrder = (Integer) CODES.get(code);
			Integer otherOrder = (Integer) CODES.get(other.code);
			return thisOrder.compareTo(otherOrder);
		}
	}

	/**
	 * <p>Indicates whether the specified object is "equal to" this
	 * <code>Stage</code>.</p>
	 *
	 * <p>Two <code>Stage</code>s are equal if they have the same order.</p>
	 *
	 * <p>A <code>Stage</code> is always equal to itself, never equal to
	 * <code>null</code>, and never equal to an object of a different class.</p>
	 *
	 * @param object the <code>Object</code> with which to compare
	 * @return <code>true</code> if <code>object</code> is not
	 *         <code>null</code>, is a <code>Stage</code>, and has the same
	 *         order as this stage code
	 * @see #compareTo(Object)
	 */
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object == null) {
			return false;
		}
		if (getClass() != object.getClass()) {
			return false;
		}

		return compareTo(object) == 0;
	}

	/**
	 * <p>Returns a hash code value for this <code>Stage</code>.</p>
	 *
	 * @return a hash code value for this <code>Stage</code>
	 */
	public int hashCode() {
		if (!code.equals(FINAL_CODE)) {
			return ((Integer) CODES.get(code)).intValue();
		} else {
			return ((Integer) Collections.max(CODES.values())).intValue() + 1;
		}
	}

	/**
	 * <p>Returns a string representation of the <code>Stage</code>.</p>
	 *
	 * @return a string representation of the <code>Stage</code>
	 */
	public String toString() {
		return code;
	}
}
