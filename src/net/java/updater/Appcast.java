/*
 * Copyright 2008 Steve Roy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.java.updater;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * <p>An appcast is an RSS 2.0 feed for the purposes of appcasting.</p>
 *
 * <p>See {@link http://cyber.law.harvard.edu/rss/rss.html} for the RSS 2.0
 * specification.</p>
 *
 * @author Steve Roy
 * @author Graham Wagener
 */
class Appcast {

	/**
	 * <p>The title of the appcast's channel.</p>
	 *
	 * <p>This corresponds to the title element of the channel element and is a
	 * required property.</p>
	 */
	private String channelTitle;

	/**
	 * <p>The URL to the website associated with the appcast.</p>
	 *
	 * <p>This corresponds to the link element of the channel element and is a
	 * required property.</p>
	 */
	private URL channelLink;

	/**
	 * <p>The description of the appcast's channel.</p>
	 *
	 * <p>This corresponds to the description element of the channel element and
	 * is a required property.</p>
	 */
	private String channelDescription;

	/**
	 * <p>The langauge the appcast's channel is written in.</p>
	 *
	 * <p>This corresponds to the language element of the channel element.</p>
	 */
	private String channelLanguage;

	/**
	 * <p>The list of the appcast's channel items.</p>
	 *
	 * <p>Each item corresponds to an item element of the channel element.</p>
	 */
	private List channelItems = new ArrayList();

	/**
	 * <p>Construct an object representing an appcast.</p>
	 *
	 * @param channelTitle       the title of the appcast's channel
	 * @param channelLink        the URL to the website associated with the
	 *                           appcast
	 * @param channelDescription the description of the appcast's channel
	 */
	public Appcast(String channelTitle, URL channelLink,
		String channelDescription) {
		this.channelTitle = channelTitle;
		this.channelLink = channelLink;
		this.channelDescription = channelDescription;
	}

	/**
	 * <p>Get the title of the appcast's channel.</p>
	 *
	 * @return the appcast's channel title
	 */
	public String getChannelTitle() {
		return channelTitle;
	}

	/**
	 * <p>Get the URL to the website associated with the appcast.</p>
	 *
	 * @return the URL of the appcast's accociated website
	 * @see    URL
	 */
	public URL getChannelLink() {
		return channelLink;
	}

	/**
	 * <p>Get the description of the appcast's channel.</p>
	 *
	 * @return the appcast's channel description
	 */
	public String getChannelDescription() {
		return channelDescription;
	}

	/**
	 * <p>Specify which language the appcast's channel is written in.</p>
	 *
	 * <p>For allowable values see the RSS 2.0 specification.</p>
	 *
	 * @param language the appcast's new channel language
	 */
	public void setChannelLanguage(String language) {
		channelLanguage = language;
	}

	/**
	 * <p>Get the language the appcast's channel is written in.</p>
	 *
	 * @return the appcast's channel language
	 */
	public String getChannelLanguage() {
		return channelLanguage;
	}

	/**
	 * <p>Add a <code>ChannelItem</code> to the appcast's channel.</p>
	 *
	 * @param channelItem the item to be added to the channel
	 * @see ChannelItem
	 */
	public void addChannelItem(ChannelItem channelItem) {
		channelItems.add(channelItem);
	}

	/**
	 * <p>Remove a <code>ChannelItem</code> from the appcast's channel.</p>
	 *
	 * @param channelItem the item to be removed from the channel
	 * @see ChannelItem
	 */
	public void removeChannelItem(ChannelItem channelItem) {
		channelItems.remove(channelItem);
	}

	/**
	 * <p>Get the number of items in the appcast's channel.</p>
	 *
	 * @return the number of the appcast's channel items
	 */
	public int getChannelItemCount() {
		return channelItems.size();
	}

	/**
	 * <p>Get the item in the appcast's channel at the specified index.</p>
	 *
	 * @param index the location in the appcast's channel of the item to
	 *              retrieve
	 * @return the <code>ChannelItem</code> at the specified index.
	 * @see ChannelItem
	 */
	public ChannelItem getChannelItem(int index) {
		return (ChannelItem) channelItems.get(index);
	}

	/**
	 * <p>Construct an <code>Appcast</code> object from an XML document.</p>
	 *
	 * <p>The XML document must be a valid RSS document and have all the
	 * required Updater appcast elements.</p>
	 *
	 * @param document the XML document to be parsed
	 * @return an <code>Appcast</code> object with its properties set to the
	 *         values from the appcast
	 * @throws ParseException if the XML document is not a valid RSS document,
	 *                        if any of the items do not contain an enclosure,
	 *                        or if a version is not formatted correctly
	 *                        according to {@link Version#Version(String)}
	 * @see Document
	 */
	public static Appcast parse(Document document) throws ParseException {
		// The root element must be rss
		Element rss = document.getDocumentElement();
		if (!rss.getNodeName().equals("rss")) {
			throw new ParseException("root element is not rss", -1);
		}

		// The version attribute of the root element must be 2.0
		if (!rss.getAttribute("version").equals("2.0")) {
			throw new ParseException("rss element has wrong version attribute",
				-1);
		}

		// The rss element must contain exactly one child element
		NodeList rssNodes = rss.getChildNodes();
		ArrayList elements = new ArrayList();
		for (int i = 0; i < rssNodes.getLength(); i++) {
			Node node = rssNodes.item(i);
			if (node instanceof Element) {
				elements.add(node);
			}
		}
		if (elements.size() != 1) {
			throw new ParseException("rss element does not have one child "
				+ "element", -1);
		}

		// The single child element must be a channel
		Element channel = (Element) elements.get(0);
		if (!channel.getNodeName().equals("channel")) {
			throw new ParseException("rss element does not have a channel "
				+ "element", -1);
		}

		String channelTitle = null; // Required
		URL channelLink = null; // Required
		String channelDescription = null; // Required
		String channelLanguage = null;
		List channelItems = new ArrayList();
		
		NodeList channelElements = channel.getElementsByTagName("*");
		for (int i = 0; i < channelElements.getLength(); i++) {
			Element channelElement = (Element) channelElements.item(i);
			String elementName = channelElement.getNodeName();
			if (elementName.equals("title")) {
				Node text = channelElement.getFirstChild();
				if (text != null) {
					channelTitle = text.getNodeValue();
				}
			} else if (elementName.equals("link")) {
				Node text = channelElement.getFirstChild();
				if (text != null) {
					try {
						channelLink = new URL(text.getNodeValue());
					} catch (MalformedURLException ex) {
						// Let the null URL be caught below
					}
				}
			} else if (elementName.equals("description")) {
				Node text = channelElement.getFirstChild();
				if (text != null) {
					channelDescription = text.getNodeValue();
				}
			} else if (elementName.equals("language")) {
				Node text = channelElement.getFirstChild();
				if (text != null) {
					channelLanguage = text.getNodeValue();
				}
			} else if (elementName.equals("item")) {
				String itemTitle = null; // Title or description is required
				String itemDescription = null; // Title or description is required
				Date itemPubDate = null;
				URL itemEnclosureURL = null; // Required attribute of an enclosure
				long itemEnclosureLength = -1; // Required attribute of an enclosure
				String itemEnclosureType = null; // Required attribute of an enclosure
				Version sparkleVersion = null; // Required for Sparkle
				Version updaterVersion = null; // Required for Updater
				Platform itemPlatform = null;
				String itemExecutable = null;
				
				DateFormat pubDateFormat = new SimpleDateFormat(
					"EEE, d MMM yyyy HH:mm:ss ZZZZZ", Locale.US);
				
				NodeList itemElements = channelElement.getElementsByTagName("*");
				for (int j = 0; j < itemElements.getLength(); j++) {
					Element itemElement = (Element) itemElements.item(j);
					elementName = itemElement.getNodeName();
					if (elementName.equals("title")) {
						Node n = itemElement.getFirstChild();
						if (n != null) {
							itemTitle = n.getNodeValue();
						}
					} else if (elementName.equals("description")) {
						Node n = itemElement.getFirstChild();
						if (n != null) {
							itemDescription = n.getNodeValue();
						}
					} else if (elementName.equals("pubDate")) {
						Node n = itemElement.getFirstChild();
						if (n != null) {
							try {
								itemPubDate =
									pubDateFormat.parse(n.getNodeValue());
							} catch (ParseException ex) {}
						}
					} else if (elementName.equals("enclosure")) {
						String value = itemElement.getAttribute("url");
						if (value.length() != 0) {
							try {
								itemEnclosureURL = new URL(value);
							} catch (MalformedURLException ex) {
								// Let the null URL be caught below
							}
						}
						value = itemElement.getAttribute("length");
						if (value.length() != 0) {
							itemEnclosureLength = Long.parseLong(value);
						}
						value = itemElement.getAttribute("type");
						if (value.length() != 0) {
							itemEnclosureType = value;
						}
						value = itemElement.getAttribute("sparkle:version");
						if (value.length() != 0) {
							sparkleVersion = new Version(value);
						}
					} else if (elementName.equals("updater:version")) {
						Node n = itemElement.getFirstChild();
						if (n != null) {
							updaterVersion = new Version(n.getNodeValue());
						}
					} else if (elementName.equals("updater:platform")) {
						Node n = itemElement.getFirstChild();
						if (n != null) {
							itemPlatform = new Platform(n.getNodeValue());
						}
					} else if (elementName.equals("updater:executable")) {
						Node n = itemElement.getFirstChild();
						if (n != null) {
							itemExecutable = n.getNodeValue();
						}
					}
				}
				
				// Give priority to our custom version, but if it's not there
				// then look for a Sparkle version
				Version version = updaterVersion;
				if (updaterVersion == null) {
					version = sparkleVersion;
				}

				if (itemTitle == null && itemDescription == null) {
					throw new ParseException("channel item does not have a "
						+ "title or description", -1);
				}
				if (itemEnclosureURL == null || itemEnclosureLength == -1
				 || itemEnclosureType == null || version == null) {
					throw new ParseException("channel item does not have a "
						+ "complete set of enclosure attributes", -1);
				}

				ChannelItem item = new ChannelItem(itemTitle, itemDescription);
				item.setPubDate(itemPubDate);
				item.setEnclosureURL(itemEnclosureURL);
				item.setEnclosureLength(itemEnclosureLength);
				item.setEnclosureType(itemEnclosureType);
				item.setVersion(version);
				item.setPlatform(itemPlatform);
				item.setExecutable(itemExecutable);
				channelItems.add(item);
			}
		}

		if (channelTitle == null
		 || channelLink == null
		 || channelDescription == null) {
			throw new ParseException("channel does not have a complete set of "
				+ "attributes", -1);
		}

		Appcast appcast = new Appcast(channelTitle, channelLink,
			channelDescription);
		appcast.setChannelLanguage(channelLanguage);
		Iterator itr = channelItems.iterator();
		while (itr.hasNext()) {
			appcast.addChannelItem((ChannelItem) itr.next());
		}

		return appcast;
	}
}
