/*
 * Copyright 2008 Steve Roy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.java.updater;

import java.net.URL;
import java.util.Date;

/**
 * A channel item is a child element of an RSS channel element. A channel may
 * contain any number of items, each corresponding to a different story, or for
 * the purposes of appcasting, different products.
 *
 * @author Steve Roy
 * @author Graham Wagener
 */
public class ChannelItem {

	/**
	 * The title of the item.
	 */
	private String title;
	
	/**
	 * The description of the item.
	 */
	private String description;
	
	/**
	 * The publication date of the item.
	 */
	private Date pubDate;
	
	/**
	 * The URL of the enclosure for the item.
	 */
	private URL enclosureURL;
	
	/**
	 * The length of the enclosure for the item.
	 */
	private long enclosureLength = -1;
	
	/**
	 * The type of the enclosure for the item.
	 */
	private String enclosureType;
	
	/**
	 * The version of the item.
	 */
	private Version version;
	
	/**
	 * The platform of the item.
	 */
	private Platform platform;
	
	/**
	 * The executable for the item.
	 */
	private String executable;
	
	/**
	 * <p>Construct a channel item.</p>
	 *
	 * @param itemTitle the title of the item
	 * @param itemDescription the description of the item
	 */
	ChannelItem(String itemTitle, String itemDescription) {
		title = itemTitle;
		description = itemDescription;
	}
	
	/**
	 * Get the title of the item. The title may be null, in which case the
	 * description will be non-null.
	 * @return the item title, or null
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Get the description of the item. The description may be null, in which
	 * case the title will be non-null.
	 * @return the item description, or null
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Set the publication date of the item.
	 * @param publicationDate the item publication date
	 */
	void setPubDate(Date publicationDate) {
		pubDate = publicationDate;
	}
	
	/**
	 * Get the publication date of the item. The publication date may be null.
	 * @return the item publication date, or null
	 */
	public Date getPubDate() {
		return pubDate;
	}
	
	/**
	 * Set the URL of the enclosure for the item.
	 * @param enclosureURL the URL of the item enclosure
	 */
	void setEnclosureURL(URL enclosureURL) {
		this.enclosureURL = enclosureURL;
	}
	
	/**
	 * Get the URL of the enclosure for the item. If the item has an enclosure,
	 * then the enclose URL, length and type are all set.
	 * @return the URL of the item enclosure, or null
	 */
	public URL getEnclosureURL() {
		return enclosureURL;
	}
	
	/**
	 * Set the length of the enclosure for the item.
	 * @param enclosureLength the length of the item enclosure
	 */
	void setEnclosureLength(long enclosureLength) {
		this.enclosureLength = enclosureLength;
	}
	
	/**
	 * Get the length of the enclosure for the item. If the item has an
	 * enclosure, then the enclose URL, length and type are all set.
	 * @return the length of the item enclosure, or -1
	 */
	public long getEnclosureLength() {
		return enclosureLength;
	}
	
	/**
	 * Set the type of the enclosure for the item. This is a MIME type.
	 * @param enclosureType the type of the item enclosure
	 */
	void setEnclosureType(String enclosureType) {
		this.enclosureType = enclosureType;
	}
	
	/**
	 * Get the type of the enclosure for the item. This is a MIME type. If the
	 * item has an enclosure, then the enclose URL, length and type are all set.
	 * @return the type of the item enclosure, or null
	 */
	public String getEnclosureType() {
		return enclosureType;
	}
	
	/**
	 * Set the version of the item.
	 * @param version the item version
	 */
	void setVersion(Version version) {
		this.version = version;
	}
	
	/**
	 * Get the version of the item. This method may not return null for a valid
	 * appcast.
	 * @return the item version
	 */
	public Version getVersion() {
		return version;
	}
	
	/**
	 * Set the platform of the item.
	 * @param platform the item platform
	 */
	void setPlatform(Platform platform) {
		this.platform = platform;
	}
	
	/**
	 * Get the platform of the item. A null platform indicates that the item is
	 * cross-platform. Otherwise the platform string should match a starts-with
	 * comparison with the current platform. For example, "Windows" would match
	 * any flavor of Windows.
	 * @return the item platform, or null
	 */
	public Platform getPlatform() {
		return platform;
	}

	/**
	 * <p>Set the executable of the item.</p>
	 *
	 * @param executable the item executable
	 */
	void setExecutable(String executable) {
		this.executable = executable;
	}

	/**
	 * <p>Get the executable of the item.  This is a path relative to the
	 * directory where the enclosure is expanded.  The path uses forward slashes
	 * as separators.</p>
	 *
	 * <p>This method may return null in which case it is up to the caller to
	 * determine the executable location through some other means.</p>
	 *
	 * @return the item executable, or null
	 */
	public String getExecutable() {
		return executable;
	}
	
	/**
	 * Get a string representation of the channel item.
	 * @return the item as a string
	 */
// NOTE: This could return the xml representation of a channel item which could
// be useful if we wanted to be able to construct and then output a channel item
// instead of just reading in and using items.
	public String toString() {
		StringBuffer b = new StringBuffer();
		b.append(getClass().getName());
		b.append(" [title=");
		b.append(title);
		b.append(",version=");
		b.append(version);
		if (platform != null) {
			b.append(",platform=");
			b.append(platform);
		}
// TODO: Add executable
		b.append("]");
		return b.toString();
	}
}
