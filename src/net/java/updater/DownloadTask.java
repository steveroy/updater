/*
 * Copyright 2008 Steve Roy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.java.updater;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * A download task transfers a given update to disk.
 *
 * @author Steve Roy
 * @author Graham Wagener
 */
class DownloadTask extends Task {

	/**
	 * Constant representing the size of one kilobyte in bytes.
	 */
	private static final int KILOBYTE = 1024;

	/**
	 * The update to download.
	 */
	private ChannelItem update;

	/**
	 * The downloaded update file.
	 */
	private File updateFile;
	
	/**
	 * <p>Construct a download task.</p>
	 *
	 * @param update the update to download
	 */
	public DownloadTask(ChannelItem update) {
		this.update = update;
	}
	
	/**
	 * <p>Execute the task's activity.</p>
	 *
	 * @return <code>true</code> if the the task completed successfully
	 * @throws IOException if an error occurs while executing the task
	 */
	public boolean execute() throws IOException {
		InputStream in = null;
		OutputStream out = null;
		File f = null;

		changeMessageKey("DownloadTask.message");
		try {
			// Get the URL where we can download the update
			URL url = update.getEnclosureURL();
			
			// Open the stream for reading the archive
			URLConnection conn = url.openConnection();
			in = conn.getInputStream();
			
			// Extract the archive name and extension from the URL
			// TODO: This doesn't work with a redirect, how to get final URL?
			String name = "archive";
			String ext = null;
			String path = url.getPath();
			int slash = path.lastIndexOf("/");
			int dot = path.lastIndexOf(".");
			if (slash != -1
			 && dot != -1
			 && slash < dot
			 && dot != path.length() - 1) {
				name = path.substring(slash + 1, dot);
				ext = path.substring(dot);
			}
			
			// Create the temporary file where to write the archive
			f = File.createTempFile(name, ext);
			out = new BufferedOutputStream(new FileOutputStream(f));
			
			// Get the size of the download
			int totalBytesRead = 0;
			changeProgressValue(totalBytesRead);
			changeProgressLength(conn.getContentLength() / KILOBYTE);
			changeProgressTextKey("DownloadTask.progressString");

			// Perform the download
			byte[] buf = new byte[KILOBYTE];
			int bytesRead;
			while ((bytesRead = in.read(buf)) != -1) {
				out.write(buf, 0, bytesRead);
				totalBytesRead += bytesRead;
				changeProgressValue(totalBytesRead / KILOBYTE);

				// Check if the task was canceled
				if (isCanceled()) {
					return false;
				}
			}
			
			updateFile = f;
			return true;
		} finally {
			// Make sure the input and output streams are closed
			if (in != null) {
				try {
					in.close();
				} catch (IOException ex) {}
			}
			if (out != null) {
				try {
					out.close();
				} catch (IOException ex) {}
			}

			// Delete the partially downloaded file
			if (isCanceled() && f != null) {
				f.delete();
			}
		}
	}
	
	/**
	 * <p>Calling a <code>DownloadTask</code>'s <code>action()</code> method
	 * will cancel the task.</p>
	 */
	public void action() {
		cancel();
	}

	/**
	 * <p>Return the property list key to the descriptive name of the task's
	 * action.</p>
	 *
	 * @return the property list key to the descriptive name of the task's
	 *         action
	 */
	public String getActionNameKey() {
		return "DownloadTask.actionName";
	}

	/**
	 * <p>The action for a <code>DownloadTask</code> should not be the default
	 * action to perform.</p>
	 *
	 * @return <code>false</code>
	 */
	public boolean isActionDefault() {
		return false;
	}

	/**
	 * Get the update file downloaded by this task.
	 * @return the downloaded update file
	 */
	public File getUpdateFile() {
		return updateFile;
	}
}
