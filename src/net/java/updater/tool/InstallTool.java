/*
 * Copyright 2008 Steve Roy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.java.updater.tool;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * The install tool is a command line tool that puts the application binaries
 * in their final location, replacing any old content, and relaunches the
 * application.
 *
 * @author Steve Roy
 * @author Graham Wagener
 */
public final class InstallTool {

	/**
	 * The version of the tool.
	 */
	private static final String VERSION = "0.5";

	/**
	 * Private constructor prevents instanciation of utility class.
	 */
	private InstallTool() {
		// Default constructor
	}

	/**
	 * Starting point of the installer tool.
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		try {
			File fromDir = null;
			File toDir = null;
			String app = null;

			// Get the command line arguments
			for (int i = 0; i < args.length; i++) {
				// TODO: Remove this after debugging
				System.out.println("arg[" + i + "] = " + args[i]);

				if (args[i].equals("-h")) {
					showUsage();
					System.exit(0);
				} else if (args[i].equals("-v")) {
					showVersion();
					System.exit(0);
				} else if (args[i].equals("-e")) {
					if (app == null && i + 1 < args.length) {
						app = args[i + 1];
					}
					i++;
				} else {
					if (fromDir == null) {
						fromDir = new File(args[i]);
					} else if (toDir == null) {
						toDir = new File(args[i]);
					}
				}
			}

			// Validate the arguments
			if (fromDir == null || toDir == null) {
				showUsage();
				System.exit(1);
			}

			// Make sure the application to be updated has quit.
			// If the application is still running at this point, it may be
			// impossible to copy over its files, especially loaded dlls on
			// Windows.  If the application is still running after the files are
			// copied, the updated version may be prevented from starting.
			// TODO: Implement checking that the updating app has quit
			// XXX: As a temporary measure, wait 1s before continuing
			// and hope that's enough.
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ex) {
			}

			// Copy the directory
			// TODO: To prevent data loss, copying should be done in multiple
			// steps by writing all new files to temp files first, then if
			// successful renaming all old files to a temp name, then if
			// successful renaming all new files to the correct name, and
			// finally if successful deleting the old files. This way, if
			// something goes wrong while we perform any of these operations we
			// can unwind the operations and get back on our feet unharmed.
			// (We'll need to keep track of the various files as we go along of
			// course.)
			copyDirectory(fromDir, toDir);

			// Launch the app
			if (app != null) {
				launchApplication(new File(toDir, app));
			}

			System.exit(0);
		} catch (IOException ex) {
			System.err.println(ex.getMessage());
			System.exit(1);
		}
	}

	/**
	 * Print the usage information of the tool to standard out.
	 */
	private static void showUsage() {
		System.out.println("Usage: install [-e executable] source_directory "
			+ "target_directory");
		System.out.println("    where executable is relative to the given "
			+ "directories");
	}

	/**
	 * Print the version information of the tool to standard out.
	 */
	private static void showVersion() {
		System.out.println("Install Tool version " + VERSION);
	}

	/**
	 * Utility method to copy a given directory to a specified target. This
	 * method recursively copies child directories as needed. If the target
	 * directory already exists, its contents is replaced atomically.
	 * @param fromDir the source directory to copy
	 * @param toDir the target directory to be created or overwritten
	 * @throws IOException if an error occurs while copying the directory
	 */
	private static void copyDirectory(File fromDir, File toDir)
		throws IOException {
		// Create the directory
		toDir.mkdir();

		// Copy the contents
		File[] fs = fromDir.listFiles();
		for (int i = 0; i < fs.length; i++) {
			File from = fs[i];
			File to = new File(toDir, from.getName());
			if (from.isDirectory()) {
				copyDirectory(from, to); // Recursive
			} else {
				copyFile(from, to);
			}
		}
	}

 	/**
 	 * Utility method to copy a given file to a specified target. If the target
	 * file already exists, it is replaced.
	 * @param fromFile the source file to copy
	 * @param toFile the target file to be created or overwritten
	 * @throws IOException if an error occurs while copying the file
 	 */
 	private static void copyFile(File fromFile, File toFile)
 		throws IOException {
 		InputStream in = null;
 		OutputStream out = null;
 		try {
 			in = new BufferedInputStream(new FileInputStream(fromFile));
 			out = new BufferedOutputStream(new FileOutputStream(toFile));

			int bytesRead;
			byte[] buf = new byte[1024];
			while ((bytesRead = in.read(buf)) != -1) {
				out.write(buf, 0, bytesRead);
			}
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException ex) {}
			}
			if (out != null) {
				try {
					out.close();
				} catch (IOException ex) {}
			}
		}
	}

 	/**
 	 * Launch a given application designated as a file.
	 * @param application the application file to launch
	 * @throws IOException if an error occurs trying to launch the application
	 */
	// NOTE: Use Platform class if included in jar.
	private static void launchApplication(File application)
		throws IOException {
		String osname = System.getProperty("os.name");
		if (osname.equals("Mac OS X")) {
			launchApplicationMac(application);
		} else if (osname.startsWith("Windows")) {
			launchApplicationWindows(application, osname);
		} else {
			launchApplicationUnix(application);
		}
	}

 	/**
 	 * Launch a given Mac application designated as a file. The file should be
	 * a valid application bundle (.app).
	 * @param application the application file to launch
	 * @throws IOException if an error occurs trying to launch the application
 	 */
 	private static void launchApplicationMac(File application)
 		throws IOException {
		// TODO: We need to make Contents/MacOS/<app> executable because this
		// information is not preserved by the zip archive, or should the
		// ExpansionTask take care of this automatically?
		// NOTE: ExpansionTask can set this, but then when InstallTool moves it
		// everything will be wiped.  However, there's no real need for the
		// files to be expanded then copied; they could just be expanded to the
		// right place originally.  This is an artifact of the design before
		// InstallTool was implemented.  To fix it, however, would take some
		// significant changes as InstallTool would need to call ExpansionTask
		// so would need to have ExpansionTask in it's jar.  I think a new
		// design that copies all of the Updater jar instead of a separate
		// InstallTool jar would work for this and it could all be done within
		// the application folder instead of in temp as it currently is.  A step
		// in this direction would be to change it so the InstallTool is put in
		// the application directory, maybe in a .update folder instead of temp.
		//exec(new String[] {"chmod", "+x",
		//	new File(application, "Contents/MacOS/MyApp").getAbsolutePath()});
		exec(new String[] {"open", application.getAbsolutePath()});
	}

 	/**
 	 * Launch a given Windows application designated as a file. The file should
	 * be a valid Windows executable (.exe).
	 * @param application the application file to launch
	 * @param osname      the name of the version of Windows the application is
	 *                    being launched on
	 * @throws IOException if an error occurs trying to launch the application
	 */
	// NOTE: This is an instance where we could use name being stored in the
	// Platform class.
	private static void launchApplicationWindows(File application,
		String osname) throws IOException {
		String cmd = "cmd.exe";
		if (osname.indexOf("95") != -1
		 || osname.indexOf("98") != -1
		 || osname.indexOf("Me") != -1) {
			cmd = "command.com";
		}
		exec(new String[] {cmd, "/c", application.getAbsolutePath()});
	}

 	/**
 	 * Launch a given Unix application designated as a file. The file should be
	 * a valid executable file.
	 * @param application the application file to launch
	 * @throws IOException if an error occurs trying to launch the application
 	 */
 	private static void launchApplicationUnix(File application)
 		throws IOException {
 		exec(new String[] {application.getAbsolutePath()});
 	}

 	/**
 	 * Utility method to execute a command in a separate process.
	 * @param command an array of tokens making up the command
	 * @throws IOException if an error occurs executing the command
 	 */
 	private static void exec(String[] command) throws IOException {
		// Execute the command
 		Process p = Runtime.getRuntime().exec(command);
		
		// Close the standard streams.  The child process is expected to outlive
		// the current one, so it is of limited use to handle any input / output
		// here.  Closing the streams ensures that the child process will not
		// get stuck waiting for the buffers to be emptied.
		p.getOutputStream().close();
		p.getInputStream().close();
		p.getErrorStream().close();
		
		// We purposely do not use the waitFor() method from the process object
		// because we want to continue execution and eventually die while the
		// new process continues
	}
}
