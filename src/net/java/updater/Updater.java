/*
 * Copyright 2008 Steve Roy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.java.updater;

import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.TimerTask;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

/**
 * An updater polls an RSS 2.0 appcast looking for available updates.
 *
 * @author Steve Roy
 * @author Graham Wagener
 */
public class Updater {

	/**
	 * Constant representing one seconds in milliseconds.
	 */
	private static final int MILLISECONDS = 1000;

	/**
	 * Property indicating a change in the ignored version. Its value is a
	 * Version object.
	 */
	public static final String IGNORED_VERSION_PROPERTY = "ignoredVersion";

	/**
	 * Property indicating a change in whether monitoring is in effect or not.
	 * Its value is a boolean.
	 */
	public static final String MONITORING_PROPERTY = "monitoring";

	/**
	 * Property indicating a change in whether monitoring is paused or not. Its
	 * value is a boolean.
	 */
	public static final String MONITORING_PAUSED_PROPERTY = "monitoringPaused";

	/**
	 * The URL of the RSS feed for the appcast to monitor.
	 */
    private String appcastURL;
	
	/**
	 * The name of the application.
	 */
    private String applicationName;
	
	/**
	 * The current version of the application.
	 */
    private Version currentVersion;
	
	/**
	 * The icon of the application.
	 */
    private Icon applicationIcon;
	
	/**
	 * The version of the update that is currently being ignored.
	 */
	private Version ignoredVersion;
	
	/**
	 * The list of objects listening for update events.
	 */
	private EventListenerList listenerList = new EventListenerList();
	
	/**
	 * Support for sending property change events.
	 */
	private PropertyChangeSupport propertyChangeSupport =
		new PropertyChangeSupport(this);

	/**
	 * The daemon timer we use to run the checks.
	 */
	private java.util.Timer timer = new java.util.Timer(true);
	
	/**
	 * The monitoring task currently scheduled by the timer.
	 */
	private TimerTask monitorTask;
	
	/**
	 * Whether monitoring is running or not.
	 */
	private boolean monitoring = false;

	/**
	 * Whether to show updates when monitoring.
	 */
	private boolean monitoringShowUpdates;
	
	/**
	 * The period interval when monitoring in seconds.
	 */
	private int monitoringPeriod;
	
	/**
	 * <p>Construct an updater.</p>
	 *
	 * <p><code>appcastURL</code> must not require authentication to view.</p>
	 *
	 * @param appcastURL the URL of the RSS feed for the appcast to monitor
	 * @param applicationName the name of the application
	 * @param currentVersion the current version of the application
	 */
    public Updater(String appcastURL, String applicationName, Version currentVersion) {
        this.appcastURL = appcastURL;
        this.applicationName = applicationName;
        this.currentVersion = currentVersion;
    }
    
	/**
	 * Get the URL of the RSS feed for the appcast to monitor.
	 * @return the URL of the appcast
	 */
    public String getAppcastURL() {
        return appcastURL;
    }
	
	/**
	 * Get the name of the application.
	 * @return the application name
	 */
	public String getApplicationName() {
		return applicationName;
	}
    
	/**
	 * Get the current version of the application.
	 * @return the application current version
	 */
    public Version getCurrentVersion() {
        return currentVersion;
    }
    
	/**
	 * Set the icon of the application.
	 * @param icon the icon of the application
	 */
	public void setApplicationIcon(Icon icon) {
		applicationIcon = icon;
	}

	/**
	 * Get the icon of the application.
	 * @return the application icon
	 */
    public Icon getApplicationIcon() {
        return applicationIcon;
    }
	
	/**
	 * Get the icon to use, whether or not the application icon is set.
	 * @return the icon
	 */
	private Icon getIcon() {
		if (applicationIcon != null) {
			return applicationIcon;
		}
		URL url = Updater.class.getResource(
			"/net/java/updater/UpdaterIcon.png");
		if (url != null) {
			Image im = Toolkit.getDefaultToolkit().getImage(url);
			MediaTracker t = new MediaTracker(new JPanel());
			t.addImage(im, 0);
			try {
				t.waitForAll();
			} catch (InterruptedException ex) {}
			return new ImageIcon(im);
		}
		return null;
	}
	
	/**
	 * Set the version of the update that is currently being ignored. It is the
	 * responsibility of the caller to persist the given version between
	 * sessions, such as in the preferences.  Passing <code>null</code> will
	 * cause no version to be ignored, i.e. all updates will be reported.
	 * @param version the ignored update version
	 */
	public void setIgnoredVersion(Version version) {
		Version oldIgnoredVersion = ignoredVersion;
		ignoredVersion = version;
		if (oldIgnoredVersion != null || ignoredVersion != null) {
			propertyChangeSupport.firePropertyChange(
				IGNORED_VERSION_PROPERTY, oldIgnoredVersion, ignoredVersion);
		}
	}
	
	/**
	 * Get the version of the update that is currently being ignored.
	 * @return the ignored update version
	 */
	public Version getIgnoredVersion() {
		return ignoredVersion;
	}
    
    /**
     * Check if updates are available, reporting errors, up-to-date messages and
	 * updates using the default user interface. This method launches a new
	 * thread that polls the appcast once for available updates asynchronously
	 * and returns immediately.
	 */
	public void checkUpdates() {
		checkUpdates(true);
	}

	/**
	 * Check if updates are available, reporting updates using the default user
	 * interface. Setting the <code>showMessages</code> parameter to false
	 * suppresses error and up-to-date messages, which is appropriate for checks
	 * performed automatically (e.g. Check for updates automatically at launch).
	 * This method launches a new thread that polls the appcast once for
	 * available updates asynchronously and returns immediately.
	 * @param showMessages whether to show error and up-to-date messages
     */
    public void checkUpdates(boolean showMessages) {
        checkUpdates(showMessages, true);
    }
    
    /**
     * Check if updates are available. Setting the <code>showMessages</code>
	 * parameter to false suppresses error and up-to-date messages, which is
	 * appropriate for checks performed automatically (e.g. Check for updates
	 * automatically at launch). Setting the <code>showUpdates</code> parameter
	 * to false suppresses the default user interface for updates as well, which
	 * is appropriate if you are implementing your own custom user interface
	 * using an {@link UpdateListener}. This method launches a new thread that
	 * polls the appcast once for available updates asynchronously and returns
	 * immediately.
	 * @param showMessages whether to show error and up-to-date messages
	 * @param showUpdates whether to show updates
     */
    public void checkUpdates(boolean showMessages, boolean showUpdates) {
		synchronized (timer) {
			timer.schedule(new CheckUpdateTask(showMessages, showUpdates), 0);
		}
    }
    
	/**
	 * <p>Get whether monitoring continuously for updates is in effect.</p>
	 *
	 * @return <code>true</code> if monitoring is in effect
	 */
	public boolean isMonitoring() {
		return monitoring;
	}

	/**
	 * <p>Get whether monitoring continuously for updates is in effect but
	 * currently paused.</p>
	 *
	 * <p>This method always returns <code>false</code> when monitoring is not
	 * in effect.</p>
	 *
	 * @return <code>true</code> if monitoring is in effect, but paused.
	 *         <code>false</code> if monitoring is not in effect or if it is in
	 *         effect and is not paused.
	 */
	public boolean isMonitoringPaused() {
		return monitoring && monitorTask == null;
	}

	/**
	 * Monitor continuously whether updates are available, reporting updates
	 * using the default user interface. Errors and up-to-date messages are
	 * always suppressed when monitoring for updates. The checks are made at the
	 * given interval of time. If monitoring is already running or paused, it is
	 * reset with the new parameters. This method launches a new thread that
	 * polls the appcast for available updates asynchronously and returns
	 * immediately.
	 * @param period the polling interval, in seconds
     */
    public void monitorUpdates(int period) {
        monitorUpdates(period, true);
    }
    
    /**
     * Monitor continuously whether updates are available. The checks are made
	 * at the given interval of time. Setting the <code>showUpdates</code>
	 * parameter to false suppresses the default user interface for updates,
	 * which is appropriate if you are implementing your own custom user
	 * interface using an {@link UpdateListener}. If monitoring is already
	 * running or paused, it is reset with the new parameters. This method
	 * launches a new thread that polls the appcast for available updates
	 * asynchronously and returns immediately.
	 * @param period the polling interval, in seconds
	 * @param showUpdates whether to show updates
     */
    public void monitorUpdates(int period, boolean showUpdates) {
		synchronized (timer) {
			// If monitoring is currently active, stop it
			stopMonitoring();

			// Restart monitoring with the new parameters
			monitoringPeriod = period;
			monitoringShowUpdates = showUpdates;
			monitoring = true; // Must be set to true before next call
			startMonitoringTask();

			propertyChangeSupport.firePropertyChange(
				MONITORING_PROPERTY, false, true);
		}
    }
	
	/**
	 * Pause monitoring whether updates are available. If monitoring is not
	 * already running and is not paused, this method has no effect.
	 */
	public void pauseMonitoring() {
		if (stopMonitoringTask()) {
			propertyChangeSupport.firePropertyChange(
				MONITORING_PAUSED_PROPERTY, false, true);
		}
	}
	
	/**
	 * Resume monitoring whether updates are available. If monitoring is not
	 * paused, this method has no effect.
	 */
	public void resumeMonitoring() {
		if (startMonitoringTask()) {
			propertyChangeSupport.firePropertyChange(
				MONITORING_PAUSED_PROPERTY, true, false);
		}
	}

	/**
	 * Stop monitoring whether updates are available. If monitoring is not
	 * already running, this method has no effect.
	 */
	public void stopMonitoring() {
		synchronized (timer) {
			boolean oldPaused = isMonitoringPaused();

			stopMonitoringTask();

			boolean oldMonitoring = monitoring;
			monitoring = false;
			propertyChangeSupport.firePropertyChange(
				MONITORING_PROPERTY, oldMonitoring, monitoring);

			// When monitoring stops, we cannot be paused anymore
			propertyChangeSupport.firePropertyChange(
				MONITORING_PAUSED_PROPERTY, oldPaused, false);
		}
	}

	/**
	 * Private method to start the timer task used by monitoring. This method
	 * can be called at any time but only has an effect if monitoring is active.
	 * It will only return true if monitoring was in effect but paused when the
	 * method was called. Otherwise this method does nothing and returns false.
	 *
	 * @return <code>true</code> if the monitoring task was started
	 */
	private boolean startMonitoringTask() {
		synchronized (timer) {
			if (monitoring && monitorTask == null) {
				monitorTask = new CheckUpdateTask(false, monitoringShowUpdates);
				timer.schedule(monitorTask,
					monitoringPeriod * MILLISECONDS,
					monitoringPeriod * MILLISECONDS);
				return true;
			}
			return false;
		}
	}

	/**
	 * Private method to stop the timer task used by monitoring. This method can
	 * be called at any time but only has an effect if monitoring is active. It
	 * will only return true if monitoring was in effect and not paused when the
	 * method was called. Otherwise this method does nothing and returns false.
	 *
	 * @return <code>true</code> if the monitoring task was stopped
	 */
	private boolean stopMonitoringTask() {
		synchronized (timer) {
			if (monitoring && monitorTask != null) {
				monitorTask.cancel();
				monitorTask = null;
				return true;
			}
			return false;
		}
	}
	
	/**
	 * Run a check for updates. This method is called by the timer task.
	 * @param showMessages whether to show error and up-to-date messages
	 * @param showUpdates whether to show updates
	 */
	private void performCheck(boolean showMessages, boolean showUpdates) {
		Appcast appcast = null;
		try {
			// Download the RSS feed
            InputStream in = new URL(appcastURL).openStream();
    		DocumentBuilder b =
    		    DocumentBuilderFactory.newInstance().newDocumentBuilder();
    		Document doc = b.parse(in);
			
			// Parse the appcast
			appcast = Appcast.parse(doc);
		} catch (Exception ex) {
			fireUpdateEvent(UpdateEvent.CHECK_FAILED, null, ex);
			if (showMessages) {
				showError(ex);
			}
			return;
		}
		
		// Find the latest update in the appcast, either marked for this
		// platform or for any platform
		Platform currentPlatform = Platform.getCurrentPlatform();
		ChannelItem latestForAny = null;
		ChannelItem latestForThis = null;
		for (int i = 0; i < appcast.getChannelItemCount(); i++) {
			ChannelItem item = appcast.getChannelItem(i);
			Platform plat = item.getPlatform();
			Version vers = item.getVersion();
			if (plat == null) {
				if (latestForAny == null
				 || vers.isGreaterThan(latestForAny.getVersion())) {
					latestForAny = item;
				}
			} else if (currentPlatform.equals(plat)) {
				if (latestForThis == null
				 || vers.isGreaterThan(latestForThis.getVersion())) {
					latestForThis = item;
				}
			}
		}
		
		// If the appcast has an update specific to this platform, we use that.
		// Otherwise we check if the appcast has an update for any platform that
		// we can use.
		ChannelItem latest = null;
		if (latestForThis != null) {
			latest = latestForThis;
		} else if (latestForAny != null) {
			latest = latestForAny;
		}

		// Check if we found something
		if (latest == null) {
			fireUpdateEvent(UpdateEvent.UPDATE_NOT_FOUND, null, null);
			if (showMessages) {
				showUpdateNotFound();
			}
			return;
		}
		
		// Check if the latest update is newer than the current version
		Version latestVersion = latest.getVersion();
		if (!latestVersion.isGreaterThan(currentVersion)) {
			fireUpdateEvent(UpdateEvent.UP_TO_DATE, latest, null);
			if (showMessages) {
				showUpToDate(latest);
			}
			return;
		}
		
		// Check if the latest update is being ignored
		// NOTE: By checking for showMessages to be false the UpdateFrame will
		// only be suppressed if all messages are suppressed.  The other option
		// would be to not show that an update is available if it has previously
		// been ignored.  There are two schools or thought on this.  The one for
		// the current functionality is that a) showMessages will only be true
		// if the user has manually called an update check, so they will want to
		// be informed of all available updates, even if they previously ignored
		// one, and b) that's what Sparkle does.  The arguments for the other
		// way are that if the user previously ignored the update they'll still
		// want to ignore it.  An example of this behaviour is Apple Software
		// Update.
//		if (latestVersion.equals(ignoredVersion)) {
		if (!showMessages && latestVersion.equals(ignoredVersion)) {
			fireUpdateEvent(UpdateEvent.UPDATE_IGNORED, latest, null);
			return;
		}
		
		// Make it known that an update is available
		fireUpdateEvent(UpdateEvent.UPDATE_AVAILABLE, latest, null);
		if (showUpdates) {
			showUpdateAvailable(latest);
		}
	}

	/**
	 * Show a message for a given exception.
	 * @param ex the exception that caused the error
	 */
	private void showError(Exception ex) {
		// We need to temporarily stop the monitoring task while the window is
		// up so we don't potentially show multiple notices
		stopMonitoringTask();

		JOptionPane.showMessageDialog(null,
			"An error occurred in retrieving update information; are you "
				+ "connected to the internet? Please try again later.",
			"Update Error",
			JOptionPane.ERROR_MESSAGE);

		// We need to restart the monitoring task if we stopped it
		startMonitoringTask();
	}
	
	/**
	 * Show a message because no updates were found.
	 */
	private void showUpdateNotFound() {
		// We need to temporarily stop the monitoring task while the window is
		// up so we don't potentially show multiple notices
		stopMonitoringTask();

		// TODO: Should this be treated as an error?
		JOptionPane.showMessageDialog(null, "No updates found.",
			"No Updates Found", JOptionPane.ERROR_MESSAGE);

		// We need to restart the monitoring task if we stopped it
		startMonitoringTask();
	}
	
	/**
	 * Show a message because the application is up-to-date.
	 * @param update the found update that is equivalent to the current version
	 */
	private void showUpToDate(ChannelItem update) {
		// We need to temporarily stop the monitoring task while the window is
		// up so we don't potentially show multiple notices
		stopMonitoringTask();

		JOptionPane.showMessageDialog(null,
			applicationName + " " + currentVersion
			+ " is currently the newest version available.", "Up-To-Date",
			JOptionPane.INFORMATION_MESSAGE);

		// We need to restart the monitoring task if we stopped it
		startMonitoringTask();
	}
	
	/**
	 * Show the built-in window to the user indicating that an update is
	 * available with options to install it or not.
	 * @param update the available update
	 */
	private void showUpdateAvailable(ChannelItem update) {
		// We need to temporarily stop the monitoring task while the window is
		// up so we don't potentially show multiple notices
		stopMonitoringTask();
		
		// Open the window on the event dispatch thread
		JFrame f = new UpdateFrame(this, applicationName, getIcon(),
			currentVersion, update);
		f.addWindowListener(new WindowAdapter() {
				public void windowClosed(WindowEvent e) {
					// We need to restart the monitoring task if we stopped it
					startMonitoringTask();
				}
			});
		SwingUtilities.invokeLater(new ShowWindowRunner(f));
	}
	
	/**
	 * Start the installation of a given update. This method launches a new
	 * thread that polls the appcast once for available updates asynchronously
	 * and returns immediately.
	 * @param update the update to install
	 */
	void installUpdate(ChannelItem update) {
		new InstallThread(update).start();
	}
	
	/**
	 * Start notifying the given listener of property change events.
	 * @param l the property change listener to add
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		propertyChangeSupport.addPropertyChangeListener(l);
	}

	/**
	 * Start notifying the given listener of change events for a given property.
	 * @param property the property of interest
	 * @param l the property change listener to add
	 */
	public void addPropertyChangeListener(String property,
		PropertyChangeListener l) {
		propertyChangeSupport.addPropertyChangeListener(property, l);
	}

	/**
	 * Stop notifying the given listener of property change events.
	 * @param l the property change listener to remove
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		propertyChangeSupport.removePropertyChangeListener(l);
	}

	/**
	 * Stop notifying the given listener of change events for a given property.
	 * @param property the property of interest
	 * @param l the property change listener to remove
	 */
	public void removePropertyChangeListener(String property,
		PropertyChangeListener l) {
		propertyChangeSupport.removePropertyChangeListener(property, l);
	}

	/**
	 * Start notifying the given listener of events occurring in the updater.
	 * @param l the update listener to add
	 */
	public void addUpdateListener(UpdateListener l) {
		listenerList.add(UpdateListener.class, l);
	}

	/**
	 * Stop notifying the given listener of events occurring in the updater.
	 * @param l the update listener to remove
	 */
	public void removeUpdateListener(UpdateListener l) {
		listenerList.remove(UpdateListener.class, l);
	}

	/**
	 * Fire an update event to all registered listeners.
	 * @param eventID the ID of the event
	 * @param item the subjected item
	 * @param exception an exception that might have occurred
	 */
	private void fireUpdateEvent(int eventID, ChannelItem item,
		Exception exception) {
		UpdateEvent e = new UpdateEvent(this, eventID, item, exception);
		Object[] ls = listenerList.getListenerList();
		for (int i = ls.length - 2; i >= 0; i -= 2) {
			if (ls[i] == UpdateListener.class) {
				switch (eventID) {
					case UpdateEvent.CHECK_FAILED:
						((UpdateListener) ls[i + 1]).checkFailed(e);
						break;
					case UpdateEvent.UPDATE_NOT_FOUND:
						((UpdateListener) ls[i + 1]).updateNotFound(e);
						break;
					case UpdateEvent.UP_TO_DATE:
						((UpdateListener) ls[i + 1]).upToDate(e);
						break;
					case UpdateEvent.UPDATE_IGNORED:
						((UpdateListener) ls[i + 1]).updateIgnored(e);
						break;
					case UpdateEvent.UPDATE_AVAILABLE:
						((UpdateListener) ls[i + 1]).updateAvailable(e);
						break;
					case UpdateEvent.APPLICATION_RESTARTING:
						((UpdateListener) ls[i + 1]).applicationRestarting(e);
						break;
					default:
				}
			}
		}
	}

	/**
	 * Implementation of the timer task that performs the check for update as
	 * scheduled.
	 */
	private class CheckUpdateTask extends TimerTask {
	
		/**
		 * Whether to show error and up-to-date messages.
		 */
        private boolean showMessages;
		
		/**
		 * Whether to show updates.
		 */
        private boolean showUpdates;
		
		/**
		 * Construct a check update task.
		 * @param showMessages whether to show error and up-to-date messages
		 * @param showUpdates whether to show updates
		 */
		public CheckUpdateTask(boolean showMessages, boolean showUpdates) {
			this.showMessages = showMessages;
			this.showUpdates = showUpdates;
		}
		
		/**
		 * Implementation of the task activity.
		 */
		public void run() {
			performCheck(showMessages, showUpdates);
		}
	}
	
	/**
	 * Implementation of the thread that performs the installation of a given
	 * update.
	 */
	private class InstallThread extends Thread {
	
		/**
		 * The update to install.
		 */
		private ChannelItem update;
		
		/**
		 * Construct an install thread.
		 * @param update the update to install
		 */
		public InstallThread(ChannelItem update) {
			super("install");
			this.update = update;
		}
		
		/**
		 * Implementation of the thread activity.
		 */
		public void run() {
			ProgressFrame f = null;
			try {
				// Open the window on the event dispatch thread
				f = new ProgressFrame(applicationName, getIcon());
				SwingUtilities.invokeLater(new ShowWindowRunner(f));
				
				// Download the update
				DownloadTask dtsk = new DownloadTask(update);
				f.setTask(dtsk);
				if (!dtsk.execute()) {
					return;
				}

				// Expand the update file
				ExpansionTask etsk = new ExpansionTask(dtsk.getUpdateFile());
				f.setTask(etsk);
				if (!etsk.execute()) {
					return;
				}

				// Install the update
				InstallationTask itsk = new InstallationTask(etsk.getFolder(),
					update);
				f.setTask(itsk);
				if (!itsk.execute()) {
					return;
				}

				// Let the listeners know we're about to restart
				fireUpdateEvent(UpdateEvent.APPLICATION_RESTARTING, null, null);
				
				// Quit
				System.exit(0);
			} catch (IOException ex) {
				f.setVisible(false);
				JOptionPane.showMessageDialog(null,
					"An error occurred: " + ex.getMessage(),
					"Installation Error",
					JOptionPane.ERROR_MESSAGE);
			} finally {
				f.setVisible(false);
				f.dispose();
			}
		}
	}
	
	/**
	 * Runnable to show a window on the event dispatch thread.
	 */
	private class ShowWindowRunner implements Runnable {
	
		/**
		 * The window to be shown.
		 */
		private Window window;
		
		/**
		 * Construct a show window runner.
		 * @param window the window to be shown
		 */
		public ShowWindowRunner(Window window) {
			this.window = window;
		}
		
		/**
		 * Implementation of the runner activity.
		 */
		public void run() {
			window.setVisible(true);
		}
	}
}
