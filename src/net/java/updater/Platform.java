/*
 * Copyright 2009 Graham Wagener
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.java.updater;

/**
 * <p>Platform is a simple class that represents an operating system platform
 * like Mac OS X or Windows.  The prupose of the class is the simplify platform
 * checking and make code more readable by avoiding conditions like
 * <code>if (!System.getProperty("os.name").startsWith("Windows"))</code>.</p>
 *
 * @author Graham Wagener
 */
public class Platform {

	/**
	 * Constant indicating the number of platforms supported by the
	 * <code>Platform</code> class.  Checking this should guarantee that all
	 * parts of the class are supporting all the platforms that other parts are.
	 */
	private static final int PLATFORMS_SUPPORTED = 3;

	/**
	 * <p>Static property storing a <code>Platform</code> object representing
	 * the operating system that the code is currently running on.  This is only
	 * set when it is accessed for the first time.</p>
	 */
	private static Platform current;

	/**
	 * <p>Property indicating whether the platform represents Mac OS X.</p>
	 */
	private boolean isMac = false;

	/**
	 * <p>Property indicating whether the platform represents a Unix system.</p>
	 */
	private boolean isUnix = false;

	/**
	 * <p>Property indicating whether the platform represents Windows.</p>
	 */
	private boolean isWindows = false;

	// NOTE: If we had an "other" category so non-Windows OSen aren't always
	// Unix (see note in constructor) then having name would be useful for
	// determining equality.
	//private String name;

	/**
	 * <p>Construct a <code>Platform</code> object for the given platform name.
	 * Names should be along the lines of "Mac OS X", "Windows 98", or
	 * "Windows".</p>
	 *
	 * @param name the name of the platform being represented
	 */
	public Platform(String name) {
		//this.name = name;
		if (name.startsWith("Windows")) {
			isWindows = true;
		} else if (!name.equals("")) {
			// NOTE: This implies that any non-Windows OS is Unix, which isn't
			// true, but is an assuption we can live with until it causes
			// problems.  We would probably have to have a set of Unixes that
			// "os.name" was checked against.  Then to differentiate between
			// them we would have to store name and compare names.
			// NOTE: Consider adding a Linux option as a Unix subset that
			// contrasts with Mac.
			isUnix = true;
			if (name.equals("Mac OS X")) {
				isMac = true;
			}
		}
	}

	/**
	 * <p>Indicates whether the specified object is "equal to" this
	 * <code>Platform</code>.</p>
	 *
	 * <p>Two <code>Platform</code>s are equal if they return the same results
	 * from calls to all <code>isPlatformX()</code> methods.</p>
	 *
	 * <p>A <code>Platform</code> is always equal to itself, never equal to
	 * <code>null</code>, and never equal to an object of a different class.</p>
	 *
	 * @param object the <code>Object</code> with which to compare
	 * @return <code>true</code> if <code>object</code> is not
	 *         <code>null</code>, is a <code>Platform</code>, and returns the
	 *         same results from calls to all <code>isPlatformX()</code> methods
	 *         as this <code>Platform</code>
	 * @see #isMac()
	 * @see #isUnix()
	 * @see #isWindows()
	 */
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object == null) {
			return false;
		}
		if (getClass() != object.getClass()) {
			return false;
		}

		Platform other = (Platform) object;
		if (other.isWindows && isWindows) {
			return true;
		}

		if (other.isMac && isMac) {
			return true;
		} else if (other.isMac != isMac) {
			// Since isUnix is a superset of isMac this prevents a Mac platform
			// being equal to a non-Mac, Unix platform in the
			// 'other.isUnix && isUnix' comparison.
			return false;
		}

		return other.isUnix && isUnix;
	}

	/**
	 * <p>Returns a hash code value for this <code>Platform</code>.</p>
	 *
	 * @return a hash code value for this <code>Platform</code>
	 */
	public int hashCode() {
		StringBuffer buffer = new StringBuffer(PLATFORMS_SUPPORTED);
		buffer.append(isMac     ? '1' : '0');
		buffer.append(isUnix    ? '1' : '0');
		buffer.append(isWindows ? '1' : '0');
		return Integer.parseInt(buffer.toString());
		// Don't need % Integer.MAX_VALUE because three digits doesn't overflow.
	}

	/**
	 * <p>Returns whether the platform represents Mac OS X.</p>
	 *
	 * <p><strong>Note:</strong> Mac OS X is a Unix system so if
	 * <code>isMac()</code> returns <code>true</code> then so will
	 * <code>isUnix()</code>.</p>
	 *
	 * @return <code>true</code> if the platform represents Mac OS X
	 */
	public boolean isMac() {
		return isMac;
	}

	/**
	 * <p>Returns whether the platform represents a Unix system.</p>
	 *
	 * @return <code>true</code> if the platform represents a Unix system
	 */
	public boolean isUnix() {
		return isUnix;
	}

	/**
	 * <p>Returns whether the platform represents Windows.</p>
	 *
	 * @return <code>true</code> if the platform represents Windows
	 */
	public boolean isWindows() {
		return isWindows;
	}

	/**
	 * <p>Returns a <code>Platform</code> object representing the operating
	 * system that the code is currently running on.</p>
	 *
	 * @return a <code>Platform</code> object representing the operating system
	 *         that the code is currently running on
	 */
	public static Platform getCurrentPlatform() {
		if (current == null) {
			current = new Platform(System.getProperty("os.name"));
		}
		return current;
	}
}
