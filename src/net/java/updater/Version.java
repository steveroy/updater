/*
 * Copyright 2008 Steve Roy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.java.updater;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>A simple, immutable implementation of a version number used for comparison
 * purposes.  This class supports version numbers in the format usually
 * preferred by most end user applications (e.g. 1.0.3b2):</p>
 *
 * <p><code>MajorVersion.MinorVersion[.BugVersion][StageCode
 * PrereleaseVersion]</code></p>
 *
 * @author Steve Roy
 * @author Graham Wagener
 */
public class Version implements Comparable {

	/**
	 * <p>Constant representing the return value of a comparison when the two
	 * values are equal.</p>
	 */
	private static final int EQUAL = 0;

	/**
	 * <p>Constant representing the return value of a comparison when the object
	 * is greater than the other object it is being compared to.  The contract
	 * of {@link Comparable#compareTo(Object)} requires this to be a positive
	 * integer.</p>
	 */
	private static final int GREATER_THAN = 1;

	/**
	 * <p>The primary version number (e.g. 1, 2, 10) representing a significant
	 * upgrade.</p>
	 */
	private int majorVersion;

	/**
	 * <p>The secondary version number (e.g. the 3 in 1.3, 4.3) representing an
	 * update.</p>
	 */
	private int minorVersion;

	/**
	 * <p>The ternary version number (e.g. the 2 in 1.0.2) representing a bug
	 * fix release.</p>
	 */
	private int bugVersion;

	/**
	 * <p>The stage of testing.</p>
	 */
	private Stage stage;

	/**
	 * <p>The version number indicating the number of the testing stage release
	 * (e.g. the 7 in 0.9a7).</p>
	 */
	private int prereleaseVersion;

	// NOTE: Consider allowing stage code without prerelease version
	/**
	 * <p>Construct a version object by parsing the given string.</p>
	 *
	 * <p>The string should be in the format
	 * <code>MajorVersion.MinorVersion[.BugVersion][StageCode
	 * PrereleaseVersion]</code> with no spaces and where items in brackets are
	 * optional and <code>StageCode</code> can be one of the representations
	 * specified in the {@link Stage} constants.</p>
	 *
	 * <p>For instance, <code>1.0.9b3</code> and <code>1.0</code> are both
	 * valid, but <code>1</code>, <code>3a2</code>, and <code>2.0b</code> are
	 * not valid.</p>
	 *
	 * @param text the string to be parsed as a version number
	 * @throws ParseException if <code>text</code> if incorrectly formatted
	 */
	public Version(String text) throws ParseException {
		// (\\d+) captures an integer, (\\p{Alpha}+) captures a character string
		String regex = "(\\d+)\\.(\\d+)(?:\\.(\\d+))?(?:(\\p{Alpha}+)(\\d+))?";
		Matcher matcher = Pattern.compile(regex).matcher(text);
		if (matcher.matches()) {
			majorVersion      = Integer.parseInt(matcher.group(1));
			minorVersion      = Integer.parseInt(matcher.group(2));
			bugVersion        = parseVersion(matcher.group(3));
			stage             = parseStage(matcher.group(4));
			prereleaseVersion = parseVersion(matcher.group(5));
		} else {
			throw new ParseException("Version string could not be parsed.", 0);
		}
	}

	/**
	 * <p>This is a simple method for returning a comparison of two
	 * <code>int</code>s.  It's purpose is to keep code cleaner by hiding the
	 * boxing.</p>
	 *
	 * @param int1 the value being compared against
	 * @param int2 the value with which to compare
	 * @return a negative integer, zero, or a positive integer if
	 *         <code>int1</code> is respectively less than, equal to, or greater
	 *         than <code>int2</code>.
	 */
	private int compare(int int1, int int2) {
		return new Integer(int1).compareTo(new Integer(int2));
	}

	/**
	 * <p>Parses a string to return a {@link Stage}.</p>
	 *
	 * @param stageCode the string to be parsed
	 * @return a parsed <code>Stage</code> or a default <code>Stage</code> if
	 *         <code>stageCode</code> is <code>null</code>
	 * @throws ParseException if stageCode is not <code>null</code>, but cannot
	 *                        be parsed
	 * @see Stage#Stage(String)
	 */
	private Stage parseStage(String stageCode) throws ParseException {
		if (stageCode == null) {
			return Stage.FINAL;
		}

		try {
			return new Stage(stageCode);
		} catch (IllegalArgumentException e) {
			ParseException pe = new ParseException("Stage code could not be"
				+ " parsed.", 0);
			pe.initCause(e);
			throw pe;
		}
	}

	/**
	 * <p>Parses a string to return a version number.</p>
	 *
	 * @param versionString the string to be parsed
	 * @return a parsed version number of a default number if
	 *         <code>versionString</code> is <code>null</code>
	 */
	private int parseVersion(String versionString) {
		if (versionString == null) {
			return 0;
		}
		return Integer.parseInt(versionString);
	}

	/**
	 * <p>Compares this <code>Version</code> for order with the specified
	 * object, which must be a <code>Version</code>.  Returns a negative
	 * integer, zero, or a positive integer if this <code>Version</code> is
	 * respectively less than, equal to, or greater than the specified
	 * object.</p>
	 *
	 * <p>If a major version is greater than the other major version this
	 * <code>Version</code> as a whole is greater, regardless of the minor
	 * version.  The order of significance of segments, greatest to least, is
	 * major version, minor version, bug version, stage, prerelease version.</p>
	 *
	 * <p>Any version is greater than <code>null</code> so a positive integer
	 * will always be returned if the specified object is <code>null</code>.</p>
	 *
	 * @param version the <code>Object</code> with which to compare
	 * @return a negative integer, zero, or a positive integer if this
	 *         <code>Version</code> is respectively less than, equal to, or
	 *         greater than <code>version</code>
	 * @throws ClassCastException if the specified object is not a
	 *                            <code>Version</code> or subclass of
	 *                            <code>Version</code>
	 */
	public int compareTo(Object version) {
		if (version == null) {
			return GREATER_THAN;
		}

		Version other = (Version) version;
		int result = compare(majorVersion, other.majorVersion);
		if (result == EQUAL) {
			result = compare(minorVersion, other.minorVersion);
		}
		if (result == EQUAL) {
			result = compare(bugVersion, other.bugVersion);
		}
		if (result == EQUAL) {
			result = stage.compareTo(other.stage);
		}
		if (result == EQUAL) {
			result = compare(prereleaseVersion, other.prereleaseVersion);
		}
		return result;
	}

	/**
	 * <p>Indicates whether the specified object is "equal to" this
	 * <code>Version</code>.</p>
	 *
	 * <p>Two <code>Version</code>s are equal if every segment, major version,
	 * minor version, bug version, stage, and prerelease version, is equal to
	 * the corresponding segment in the other <code>Version</code>.</p>
	 *
	 * <p>A <code>Version</code> is always equal to itself, never equal to
	 * <code>null</code>, and never equal to an object of a different class.</p>
	 *
	 * @param object the <code>Object</code> with which to compare
	 * @return <code>true</code> if <code>object</code> is not
	 *         <code>null</code>, is a <code>Version</code>, and all the
	 *         segments are the same as the corresponding segments in this
	 *         version.  <code>false</code> if <code>object</code> is
	 *         <code>null</code>, is not a <code>Version</code>, or any of the
	 *         segments are not the same.
	 */
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object == null) {
			return false;
		}
		if (getClass() != object.getClass()) {
			return false;
		}

		return compareTo(object) == 0;
	}

	/**
	 * <p>Returns a hash code value for this <code>Version</code>.</p>
	 *
	 * @return a hash code value for this <code>Version</code>
	 */
	public int hashCode() {
		return Integer.parseInt("" + majorVersion + minorVersion + bugVersion
			+ stage.hashCode() + prereleaseVersion) % Integer.MAX_VALUE;
	}

	/**
	 * <p>This is a convenience method that returns <code>true</code> if
	 * {@link #compareTo(Object)} returns a positive integer.</p>
	 *
	 * @param version the <code>Version</code> with which to compare
	 * @return <code>true</code> if this <code>Version</code> is greater than
	 *         the given <code>Version</code>, <code>false</code> otherwise
	 */
	public boolean isGreaterThan(Version version) {
		return compareTo(version) > 0;
	}

	/**
	 * <p>Returns a string representation of the <code>Version</code>.  The
	 * format of the string is
	 * <code>MajorVersion.MinorVersion[.BugVersion][StageCode
	 * PrereleaseVersion]</code> with no spaces.  <code>BugVersion</code> will
	 * only be included if it is not 0.  Examples of string representations
	 * are:</p>
	 *
	 * <ul>
	 *   <li><code>2.0</code></li>
	 *   <li><code>1.0a7</code></li>
	 *   <li><code>3.7.5</code></li>
	 * </ul>
	 *
	 * @return a string representation of the <code>Version</code>
	 */
	public String toString() {
		StringBuffer b = new StringBuffer();
		b.append(majorVersion);
		b.append(".");
		b.append(minorVersion);
		if (bugVersion > 0) {
			b.append(".");
			b.append(bugVersion);
		}
		if (!stage.equals(Stage.FINAL)) {
			b.append(stage);
			b.append(prereleaseVersion);
		}
		return b.toString();
	}
}
