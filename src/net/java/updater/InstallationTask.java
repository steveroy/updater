/*
 * Copyright 2008 Steve Roy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.java.updater;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.StringTokenizer;

import net.java.updater.tool.InstallTool;

/**
 * An installation task determines data needed for a given update and sets up an
 * InstallTool to run when the application quits.
 *
 * @author Steve Roy
 * @author Graham Wagener
 */
class InstallationTask extends Task {

	/**
	 * The folder containing the expanded update.
	 */
	private File updateFolder;
	
	/**
	 * The appcast item representing the update being installed.
	 */
	private ChannelItem update;

	/**
	 * <p>Construct an installation task.</p>
	 *
	 * @param updateFolder the folder containing the expanded update
	 * @param update the appcast item representing the update being installed
	 */
	public InstallationTask(File updateFolder, ChannelItem update) {
		this.updateFolder = updateFolder;
		this.update = update;
	}
	
	/**
	 * <p>Execute the task's activity.</p>
	 *
	 * @return <code>true</code> if the task completed successfully
	 * @throws IOException if an error occurs while executing the task
	 */
	public boolean execute() throws IOException {
		// Ask the user when to install and restart
		changeMessageKey("InstallationTask.readyMessage");
		changeProgressLength(100);
		changeProgressValue(100);
		changeProgressTextKey(null);

		// NOTE: At this point the only way to cancel the update is to close the
		// window.  However, it is not clear that cancelling is still an option
		// or that closing the window will cancel.  Is there some way to inform
		// the user of this?

		// Wait for the user to press the button. Execution of the thread will
		// resume when the action() method is called, which resumes the thread.
		synchronized (this) {
			try {
				wait();
			} catch (InterruptedException ex) {}
		}
		
		// Make the progress bar indeterminate so the user sees that something
		// is actually happening and disable the button since the operation
		// cannot be canceled at this point
		// NOTE: ...except by closing the window, as noted above.  Should the
		// ability to cancel actually be disabled at this point?
		changeMessageKey("InstallationTask.installMessage");
		changeProgressLength(0); // Indeterminate
		changeInteractivity(false);

		// Get the location of the application on disk
		File appFolder = findApplicationFolder();
		if (appFolder == null) {
			throw new IOException("Location of application on disk could not "
				+ "be determined");
		}

		// Figure out how the application is launched
		File app = null;
		String path = update.getExecutable();
		if (path != null) {
			// The developer chose to override the default search behavior
			app = buildApplicationExecutable(updateFolder, path);
		} else {
			// The developer didn't specify the path in the appcast so let's try
			// to determine the executable the good old fashion way
			app = findApplicationExecutable(updateFolder,
				createExecutableFilter());
		}
		if (app == null) {
			throw new IOException("Executable file could not be determined");
		}
		String appPath = app.getAbsolutePath().substring(
			updateFolder.getAbsolutePath().length() + 1);

		// Extract into tmp the installation tool
		File f = File.createTempFile("Tool", ".jar");
		InputStream in = null;
		OutputStream out = null;
		try {
			in = getClass().getResourceAsStream("tool/Tool.jar");
			out = new BufferedOutputStream(new FileOutputStream(f));
			int bytesRead;
			byte[] buffer = new byte[1024];
			while ((bytesRead = in.read(buffer)) != -1) {
				out.write(buffer, 0, bytesRead);
			}
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException ex) {}
			}
			if (out != null) {
				try {
					out.close();
				} catch (IOException ex) {}
			}
		}

		// Install the shutdown hook that will launch the installer upon quit
		Runtime.getRuntime().addShutdownHook(new LaunchToolThread(
			f, updateFolder, appFolder, appPath));
		
		return true;
	}
	
	/**
	 * Find the folder containing the current version of the application. If it
	 * cannot be determined, this method returns null.
	 * @return the application folder, or null
	 */
	private File findApplicationFolder() {
		// Find the file on disk that contains this class
		File me = findThisFile();
		if (me == null) {
			return null;
		}

		// Find a file with the same name in the update folder so we can use it
		// as a reference point for the folder comparison
		File updateMe = findFileByName(me.getName(), updateFolder);
		if (updateMe == null) {
			return null;
		}

		// Determine the root folder of the application by marching up the
		// two folders in parallel until we have reached the update folder
		return findTopFolder(me, updateMe, updateFolder);
	}
	
	/**
	 * Find the file that contains this class. If it cannot be determined, this
	 * method returns null.
	 * @return the current file, or null
	 */
	private File findThisFile() {
		// Get the full path to the class as a URL. This should return a URL of
		// the form jar:file://path/to/the/file.jar!/fully/qualified/name.class.
		// We rely on this behavior to determine the full path of this file. If
		// this cannot be guaranteed for a desktop application or is not
		// portable across platforms, another mechanism will be needed, either
		// to replace this one, or to supplement it.
		Class clss = getClass();
		String name = "/" + clss.getName().replace('.', '/') + ".class";
		URL url = clss.getResource(name);
		
		// Turn the URL into an actual, platform-conformant, file path.
		
		// We want to work with a file: URL so let's drop any other protocol
		// identifier we find at the beginning of the URL.
		// NOTE: This relies on the assumption that there's nothing else
		// important to the URI/URL than the path.
		String path;
		if (!url.getProtocol().equals("file")) {
			path = url.getPath();
		} else {
			path = url.toString();
		}

		// We don't need the part of the path that identifies the fully
		// qualified class name within the jar so let's drop anything we find
		// starting at the bang
		int pos = path.indexOf("!");
		if (pos != -1) {
			path = path.substring(0, pos);
		}

		try {
			// Converting the path to a URI then to a File removes the 'file:'
			// prefix and will un-escape spaces and other characters that are
			// illegal in URLs.
			return new File(new URI(path));
		} catch (URISyntaxException ex) {
			return null;
		}
	}
	
	/**
	 * Find a file with a given name in a given folder hierarchy. The folder
	 * hierarchy is searched recursively until an exact match, including case,
	 * is found. If multiple matches exist, only the first match is returned. If
	 * no match is found, this method returns null.
	 * @param fileName the name of the file to look for
	 * @param folder the folder to search
	 * @return the file found, or null
	 */
	private static File findFileByName(String fileName, File folder) {
		File[] fs = folder.listFiles();
		for (int i = 0; i < fs.length; i++) {
			File f = fs[i];
			if (f.isFile() && f.getName().equals(fileName)) {
				return f;
			} else if (f.isDirectory()) {
				File f2 = findFileByName(fileName, f); // Recursive
				if (f2 != null) {
					return f2;
				}
			}
		}
		return null;
	}
	
	/**
	 * March up the folder hierarchy of two given files in parallel, making sure
	 * parent names match, until the top folder for the first file is found that
	 * matches the given top folder for the second file. The names of the top
	 * folders need not match, only their contents is important. If something
	 * doesn't match during this inspection, this method returns null.
	 * @param file1 the file whose parent we need to find
	 * @param file2 the file equivalent to the first file and whose parent is
	 *              known
	 * @param folder2 the top folder containing the second file
	 * @return the top folder of the first file, or null
	 */
	private static File findTopFolder(File file1, File file2, File folder2) {
		File f1 = file1;
		File f2 = file2;
		while (f2 != null && !f2.equals(folder2)) {
			// If the folder names don't match, this could indicate a problem so
			// we bail out. But is this the right thing to do? It could just be
			// that the directory structure of the app has changed, and by not
			// allowing such structure changes, we're preventing the update from
			// working for those cases, but how else can we find the top folder
			// of the app?
//			if (!f1.getName().equals(f2.getName()))
//				return null;
			
			f1 = f1.getParentFile();
			f2 = f2.getParentFile();
		}
		return f1;
	}
	
	/**
	 * Construct a file object from a given folder and a path relative to it.
	 * @param folder       the folder to which the path is relative
	 * @param relativePath the relative path from the folder, using forward
	 *                     slashes
	 * @return the executable
	 */
	private static File buildApplicationExecutable(File folder,
		String relativePath) {
		File f = folder;
		StringTokenizer st = new StringTokenizer(relativePath, "/");
		while (st.hasMoreTokens()) {
			String tok = st.nextToken();
			f = new File(f, tok);
		}
		return f;
	}

	/**
	 * <p>Find an application executable in a given folder hierarchy based on
	 * given filter.</p>
	 *
	 * <p>A breadth-first search is run over fhe folder hierarchy until an
	 * executable is found.  If multiple executables exist, only the first one
	 * is returned.  If none is found, this method returns null.</p>
	 *
	 * @param folder the folder to search
	 * @param filter the filter used to determine if a file is executable
	 * @return the executable found, or null
	 */
	private static File findApplicationExecutable(File folder,
		ExecutableFilter filter) {
		File f;
		File[] children;
		// This could be a Queue in a newer version of Java.
		LinkedList queue;

		queue = new LinkedList();
		queue.add(folder);
		while (!queue.isEmpty()) {
			f = (File) queue.removeFirst();
			if (filter.isFileExecutable(f)) {
				return f;
			}
			children = f.listFiles();
			if (children != null) {
				queue.addAll(Arrays.asList(children));
			}
		}
		return null;
	}
	
	/**
	 * Create an instance of an executable filter appropriate for the current
	 * platform.
	 * @return an executable filter
	 */
	private static ExecutableFilter createExecutableFilter() {
		Platform currentPlatform = Platform.getCurrentPlatform();
		if (currentPlatform.isMac()) {
			return new MacChecker();
		} else if (currentPlatform.isWindows()) {
			return new WindowsChecker();
		} else {
			return new UnixChecker();
		}
	}
	
	/**
	 * <p>This is a method for interacting with a task, usually when it is
	 * running.</p>
	 *
	 * <p>Each task is free to implement this action as needed.</p>
	 */
	// NOTE: Can't say the action simply cancels the task like for the others as
	// it state dependent.
	// TODO: Determine a way of explaining what action does.
	// TODO: While the task says it is not interactive until a point where it
	// needs to be notified action() could still be called.  Determine what the
	// appropriate behavior should be and implement it.  This might involve
	// having an property that stores the task's interactive state and testing
	// it here.  Possibly have it in Task and modified by changeInteractivity().
	// Task.action() could be concrete and overridden by all the subclasses.
	// Then in Task.action() it could check for interactivity and return false,
	// the subclass.action() tests the return and then either returns or does
	// the action.  The contract that action() always checks the interactivity
	// would have to be stated and obeyed by all the subclasses (by calling
	// super and behaving appropriately).  Of course this isn't needed
	// currently, so KISS?  This is especially meaningless for the other
	// subclasses because for them the action is cancel() and tasks can always
	// be canceled (just close the progress window).
	// If it's only done for this task leave a note of these ideas.  Locally
	// changeInteractivity() could be overridden to set a local property as well
	// as do just a simple call to super.  Of course, this all depends on the
	// determination of the appropriate behavior.
	public void action() {
		// Resume execution of the task thread which is in the waiting state
		synchronized (this) {
			notify();
		}
	}

	/**
	 * <p>Return the property list key to the descriptive name of the task's
	 * action.</p>
	 *
	 * @return the property list key to the descriptive name of the task's
	 *         action
	 */
	public String getActionNameKey() {
		return "InstallationTask.actionName";
	}

	/**
	 * <p>The action for an <code>InstallationTask</code> should be the default
	 * action to perform (when the task is interactive).</p>
	 *
	 * @return <code>true</code>
	 */
	public boolean isActionDefault() {
		return true;
	}

	/**
	 * <p>An <code>ExecutableFilter</code> can be queried as to whether a given
	 * file is executable.</p>
	 *
	 * <p>Classes implementing the <code>ExecutableFilter</code> interface are
	 * generally platform specific filters.</p>
	 */
	private static interface ExecutableFilter {
		/**
		 * <p>Return whether a given file is executable according to the
		 * criteria of the implementing class.</p>
		 *
		 * @param file the file to be checked
		 * @return <code>true</code> if the file is executable
		 */
		boolean isFileExecutable(File file);
	}

	/**
	 * <p>This implementation of <code>ExecutableFilter</code> determines
	 * whether files are executable on Mac OS X.</p>
	 */
	private static class MacChecker implements ExecutableFilter {
		/**
		 * <p>Return whether a given file is a Mac executable.</p>
		 *
		 * <p>This currently only accepts applications with the .app suffix.</p>
		 *
		 * @param file the file to be checked
		 * @return <code>true</code> if the file is executable
		 */
		public boolean isFileExecutable(File file) {
			// NOTE: There can be Mac applications that are not folders, e.g.
			// AppleScript apps or Carbon apps.  Most likely Java apps will be
			// folders because they will have been made with JarBundler, but it
			// should still be noted.  The directory check has been commented
			// for testing, not as a definitive choice against it.
			return /*file.isDirectory() && */file.getName().endsWith(".app");
		}
	}

	/**
	 * <p>This implementation of <code>ExecutableFilter</code> determines
	 * whether files are executable on Windows.</p>
	 */
	private static class WindowsChecker implements ExecutableFilter {
		/**
		 * <p>Return whether a given file is a Windows executable.</p>
		 *
		 * @param file the file to be checked
		 * @return <code>true</code> if the file is executable
		 */
		public boolean isFileExecutable(File file) {
			return file.isFile() && file.getName().endsWith(".exe");
		}
	}

	/**
	 * <p>This implementation of <code>ExecutableFilter</code> determines
	 * whether files are executable on Unix.</p>
	 */
	private static class UnixChecker implements ExecutableFilter {
		/**
		 * <p>Return whether a given file is a Unix executable.</p>
		 *
		 * <p>This currently only accepts shell scripts with the .sh suffix.</p>
		 *
		 * @param file the file to be checked
		 * @return <code>true</code> if the file is executable
		 */
		public boolean isFileExecutable(File file) {
			// NOTE: Should this take into account file permissions?
			return file.isFile() && file.getName().endsWith(".sh");
		}
	}

	/**
	 * <p>The seperate thread used to run {@link InstallTool}.</p>
	 */
	private static class LaunchToolThread extends Thread {
		private File jar;
		private File updateFolder;
		private File applicationFolder;
		private String applicationPath;

		/**
		 * <p>Construct an <code>InstallTool</code> launching thread.</p>
		 *
		 * @param jar               the external jar file of
		 *                          <code>InstallTool</code> to run
		 * @param updateFolder      the folder containing the new version of the
		 *                          application to be updated
		 * @param applicationFolder the folder containing the existing version
		 *                          of the application to be updated
		 * @param applicationPath   the path to the application executable for
		 *                          relaunching it once it is updated
		 */
		public LaunchToolThread(File jar, File updateFolder,
			File applicationFolder, String applicationPath) {
			this.jar = jar;
			this.updateFolder = updateFolder;
			this.applicationFolder = applicationFolder;
			this.applicationPath = applicationPath;
		}

		/**
		 * <p>Launch <code>InstallTool</code> and forget about it.</p>
		 */
		public void run() {
			System.out.println("updateFolder=" + updateFolder);
			System.out.println("applicationFolder=" + applicationFolder);
			System.out.println("applicationPath=" + applicationPath);
			
			try {
				Process p = Runtime.getRuntime().exec(new String[] {
					"java", "-jar", jar.getAbsolutePath(),
					"-e", applicationPath,
					updateFolder.getAbsolutePath(),
					applicationFolder.getAbsolutePath()});

				// NOTE: Since the application is about to quit these gobblers
				// are of limited use.  The only reason this doesn't lead to
				// errors is that InstallTool has limited output.  Just to be
				// safe should the streams be closed as they are in InstallTool?
				new StreamGobbler(p.getInputStream()).start();
				new StreamGobbler(p.getErrorStream()).start();

				// The program is in a shutdown hook so should quit with all
				// haste from this point.  No calls should be made in regards to
				// waiting for the child process or a deadlock will occur.
				// NOTE: Only once blocking is implemented in InstallTool
			} catch (IOException ex) {
				System.err.println(ex.getMessage());
			}
		}
	}

	/**
	 * This is used to drain streams without interrupting the program.  For an
	 * explanation and the source of the name see:
	 * http://www.javaworld.com/javaworld/jw-12-2000/jw-1229-traps.html?page=1
	 *
	 * Upgrading to Java 7 will make this unnecessary since ProcessBuilder will
	 * be able to specify where the System streams are directed.
	 */
	// NOTE: See note where StreamGobbler is used.  If it is removed then move
	// the comment about Java 7 to where the streams are closed.
	private static class StreamGobbler extends Thread {
		private InputStream in;

		public StreamGobbler(InputStream in) {
			this.in = in;
		}

		public void run() {
			BufferedReader reader;
			reader = new BufferedReader(new InputStreamReader(in));
			String line = null;
			try {
				while ((line = reader.readLine()) != null) {
					System.out.println(line);
				}
			} catch (IOException ex) {
				System.err.println(ex.getMessage());
			}
		}
	}
}
