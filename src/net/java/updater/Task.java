/*
 * Copyright 2008 Steve Roy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.java.updater;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;

/**
 * <p>Abstract implementation of a task that can be executed and canceled.</p>
 *
 * @author Steve Roy
 * @author Graham Wagener
 */
abstract class Task {

	/**
	 * <p>Property indicating a change in whether the task's action should be
	 * able to be performed.  Its value is a boolean.</p>
	 */
	public static final String INTERACTIVE_PROPERTY = "interactive";

	/**
	 * <p>Property indicating a change in the task's explanatory message.  Its
	 * value is a key in a property list.</p>
	 */
	public static final String MESSAGE_KEY_PROPERTY = "messageKey";

	/**
	 * <p>Property indicating a change in the task's numerical end value.  Its
	 * value is a long.</p>
	 */
	public static final String PROGRESS_LENGTH_PROPERTY = "progressLength";

	/**
	 * <p>Property indicating a change in the text description of the task's
	 * progress.  Its value is a key in a property list.</p>
	 *
	 * <p><strong>Note:</strong> If property list values are format strings with
	 * format elements, the first two elements will be progress value and
	 * progress length.  All other format elements will be unset.</p>
	 */
	public static final String PROGRESS_TEXT_KEY_PROPERTY = "progressTextKey";

	/**
	 * <p>Property indicating a change in the numerical value of the task's
	 * progress.  Its value is a long.</p>
	 */
	public static final String PROGRESS_VALUE_PROPERTY = "progressValue";

	/**
	 * <p>Support for sending property change events.</p>
	 */
	private PropertyChangeSupport propertyChangeSupport =
		new PropertyChangeSupport(this);

	/**
	 * <p>Whether the task has been canceled.</p>
	 */
	private boolean canceled = false;

	/**
	 * <p>This is a method for interacting with a task, usually when it is
	 * running.</p>
	 *
	 * <p>Each task is free to implement this action as needed.</p>
	 */
	public abstract void action();

	/**
	 * <p>Return the property list key to the descriptive name of the task's
	 * action.</p>
	 *
	 * @return the property list key to the descriptive name of the task's
	 *         action
	 */
	public abstract String getActionNameKey();

	/**
	 * <p>Return whether the task's action should be the default action to
	 * perform or not.</p>
	 *
	 * @return <code>true</code> if the task's action should be the default
	 */
	public abstract boolean isActionDefault();

	/**
	 * <p>Cancel the task.</p>
	 */
	// NOTE: Should this somehow interrupt the current thread instead of having
	// isCancelled() checks everywhere?
	public void cancel() {
		canceled = true;
	}
	
	/**
	 * <p>Return whether the task has been canceled or not.</p>
	 *
	 * @return <code>true</code> if the task was canceled
	 */
	public boolean isCanceled() {
		return canceled;
	}

	/**
	 * <p>Start notifying the given listener of property change events.</p>
	 *
	 * @param l the property change listener to add
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		propertyChangeSupport.addPropertyChangeListener(l);
	}

	/**
	 * <p>Stop notifying the given listener of property change events.</p>
	 *
	 * @param l the property change listener to remove
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		propertyChangeSupport.removePropertyChangeListener(l);
	}

	/**
	 * <p>Execute the task's activity.</p>
	 *
	 * @return <code>true</code> if the task completed successfully
	 * @throws IOException if an error occurs while executing the task
	 */
	abstract boolean execute() throws IOException;

	/**
	 * <p>Convenience method to fire a {@link PropertyChangeEvent} for the
	 * {@link #INTERACTIVE_PROPERTY} with the provided boolean for the new
	 * value.</p>
	 *
	 * @param interactive the new value of <code>INTERACTIVE_PROPERTY</code>
	 */
	protected void changeInteractivity(boolean interactive) {
		propertyChangeSupport.firePropertyChange(Task.INTERACTIVE_PROPERTY,
			null, Boolean.valueOf(interactive));
	}

	/**
	 * <p>Convenience method to fire a {@link PropertyChangeEvent} for the
	 * {@link #MESSAGE_KEY_PROPERTY} with the provided string for the new
	 * value.</p>
	 *
	 * @param message the new value of <code>MESSAGE_KEY_PROPERTY</code>
	 */
	protected void changeMessageKey(String message) {
		propertyChangeSupport.firePropertyChange(MESSAGE_KEY_PROPERTY, null,
			message);
	}

	/**
	 * <p>Convenience method to fire a {@link PropertyChangeEvent} for the
	 * {@link #PROGRESS_LENGTH_PROPERTY} with the provided integer for the new
	 * value.</p>
	 *
	 * @param length the new value of <code>PROGRESS_LENGTH_PROPERTY</code>
	 */
	protected void changeProgressLength(int length) {
		propertyChangeSupport.firePropertyChange(PROGRESS_LENGTH_PROPERTY, null,
			new Integer(length));
	}

	/**
	 * <p>Convenience method to fire a {@link PropertyChangeEvent} for the
	 * {@link #PROGRESS_TEXT_KEY_PROPERTY} with the provided string for the new
	 * value.</p>
	 *
	 * @param progressString the new value of
	 *                       <code>PROGRESS_TEXT_KEY_PROPERTY</code>
	 */
	protected void changeProgressTextKey(String progressString) {
		propertyChangeSupport.firePropertyChange(PROGRESS_TEXT_KEY_PROPERTY,
			null, progressString);
	}

	/**
	 * <p>Convenience method to fire a {@link PropertyChangeEvent} for the
	 * {@link #PROGRESS_VALUE_PROPERTY} with the provided integer for the new
	 * value.</p>
	 *
	 * @param value the new value of <code>PROGRESS_VALUE_PROPERTY</code>
	 */
	protected void changeProgressValue(int value) {
		propertyChangeSupport.firePropertyChange(PROGRESS_VALUE_PROPERTY, null,
			new Integer(value));
	}
}
