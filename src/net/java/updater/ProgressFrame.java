/*
 * Copyright 2008 Steve Roy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.java.updater;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

/**
 * <p>Implementation of the default user interface for displaying the
 * installation progress.</p>
 *
 * @author Steve Roy
 * @author Graham Wagener
 */
class ProgressFrame extends JFrame implements PropertyChangeListener {

	/**
	 * <p>The message label.</p>
	 */
	private JLabel messageLabel;
	
	/**
	 * <p>The progress bar.</p>
	 */
	private JProgressBar progressBar;
	
	/**
	 * <p>The label for the progress text.</p>
	 */
	private JLabel progressLabel;

	/**
	 * <p>The localized pattern for generating the progress text.</p>
	 *
	 * @see MessageFormat
	 */
	private String progressLabelFormat = null;

	/**
	 * <p>The single dialog button.</p>
	 */
	private JButton button;

	/**
	 * <p>The task for which the progress is being displayed.</p>
	 */
	private Task task;

	/**
	 * <p>The current value of the progress bar.</p>
	 */
	private int progressValue = 0;

	/**
	 * <p>The end value of the progress bar.</p>
	 */
	private int progressLength = 0;
	
	/**
	 * <p>The localized resources to use.</p>
	 */
	private ResourceBundle i18n;

	/**
	 * <p>Construct a progress frame.</p>
	 *
	 * @param applicationName the name of the application
	 * @param applicationIcon the icon of the application
	 */
	public ProgressFrame(String applicationName, Icon applicationIcon) {
		super();
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setResizable(false);
		addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					if (task != null) {
						task.cancel();
					}
					setVisible(false);
					dispose();
				}
			});

		i18n = ResourceBundle.getBundle("net.java.updater.Strings");

		setTitle(MessageFormat.format(
			i18n.getString("ProgressFrame.title"),
			applicationName));

		JPanel c = (JPanel) getContentPane();
		c.setBorder(BorderFactory.createEmptyBorder(18, 18, 18, 18));
		c.setLayout(new GridBagLayout());

		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridy = 0;
		
		gbc.gridx = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 3;
		gbc.anchor = GridBagConstraints.NORTH;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0;
		gbc.insets.top = 0;
		gbc.insets.left = 0;
		JLabel icon = new JLabel(applicationIcon);
		c.add(icon, gbc);
		
		gbc.gridx = 1;
		gbc.gridwidth = 2;
		gbc.gridheight = 1;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 100;
		gbc.insets.top = 0;
		gbc.insets.left = 20;
        messageLabel = new JLabel();
		messageLabel.setFont(messageLabel.getFont().deriveFont(Font.BOLD));
        c.add(messageLabel, gbc);
        
		gbc.gridy = 1;
		
		gbc.gridx = 1;
		gbc.gridwidth = 2;
		gbc.gridheight = 1;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 100;
		gbc.insets.top = 8;
		gbc.insets.left = 20;
        progressBar = new JProgressBar(JProgressBar.HORIZONTAL, 0, 100);
        c.add(progressBar, gbc);
        
		gbc.gridy = 2;
		
		gbc.gridx = 1;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 100;
		gbc.insets.top = 8;
		gbc.insets.left = 20;
        progressLabel = new JLabel();
        c.add(progressLabel, gbc);
		
		gbc.gridx = 2;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0;
		gbc.insets.top = 8;
		gbc.insets.left = 20;
		button = new JButton();
		button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (task != null) {
						task.action();
					}
				}
			});
		c.add(button, gbc);
		
		setSize(384, 129);
		
		Dimension ss = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension s = getSize();
	    setLocation((ss.width - s.width) / 2, (ss.height - s.height) / 2);
	}

	/**
	 * <p>React to changes to the frame's task's properties.</p>
	 *
	 * @param e the event describing the property that has changed
	 */
	public void propertyChange(PropertyChangeEvent e) {
		Object value = e.getNewValue();
		String prop = e.getPropertyName();
		if (prop.equals(Task.INTERACTIVE_PROPERTY)) {
			setButtonEnabled(((Boolean) value).booleanValue());
		} else if (prop.equals(Task.MESSAGE_KEY_PROPERTY)) {
			setMessage(i18n.getString((String) value));
		} else if (prop.equals(Task.PROGRESS_LENGTH_PROPERTY)) {
			setProgressLength(((Integer) value).intValue());
		} else if (prop.equals(Task.PROGRESS_TEXT_KEY_PROPERTY)) {
			if (value == null) {
				progressLabelFormat = null;
			} else {
				progressLabelFormat = i18n.getString((String) value);
			}
			setProgressText();
		} else if (prop.equals(Task.PROGRESS_VALUE_PROPERTY)) {
			setProgressValue(((Integer) value).intValue());
		}
	}

	/**
	 * <p>Set the task for which the progress will be displayed.</p>
	 *
	 * @param newTask the task for which progress will be displayed
	 * @see Task
	 */
	public void setTask(Task newTask) {
		if (task != null) {
			task.removePropertyChangeListener(this);
		}
		task = newTask;
		if (task != null) {
			task.addPropertyChangeListener(this);
			setButton(i18n.getString(task.getActionNameKey()),
				task.isActionDefault());
		}
	}

	/**
	 * <p>Asynchronously set the message to display on the frame.</p>
	 *
	 * @param message the new message to display on the frame.
	 */
	private void setMessage(final String message) {
		SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					messageLabel.setText(message);
				}
			});
	}

	/**
	 * <p>Asynchronously set the numerical end value of the progress bar.</p>
	 *
	 * @param length the new numerical end value of the progress bar.
	 */
	private void setProgressLength(int length) {
		progressLength = length;

		setProgressText();
		SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					// TODO: Make a clearer method in the ProgressFrame class to
					// make the progress bar indeterminate
					progressBar.setIndeterminate(progressLength == 0);
				}
			});
	}

	/**
	 * <p>Asynchronously set the current numerical value of the progress
	 * bar.</p>
	 *
	 * @param value the new numerical value of the progress bar.
	 */
	private void setProgressValue(int value) {
		progressValue = value;

		setProgressText();
		SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					if (progressLength == 0) {
						progressBar.setValue(0);
					} else {
						progressBar.setValue((int)
							(100L * progressValue / progressLength));
					}
				}
			});
	}

	/**
	 * <p>Asynchronously update the textual representation of the progress.</p>
	 */
	private void setProgressText() {
		// NOTE: Gets called whenever progressLabelFormat, progressValue, or
		// progressLength change.
		final String text;

		if (progressLabelFormat == null) {
			text = null;
		} else {
			text = MessageFormat.format(progressLabelFormat,
                String.valueOf(progressValue),
				String.valueOf(progressLength));
		}

		SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					progressLabel.setText(text);
				}
			});
	}

	/**
	 * <p>Asynchronously set the text of the button and the button as the
	 * default button or not.</p>
	 *
	 * @param text   the new text for the button
	 * @param dfault <code>true</code> for the button to be the default,
	 *               <code>false</code> to not be the default
	 */
	private void setButton(final String text, final boolean dfault) {
		SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					button.setText(text);
					if (dfault) {
						getRootPane().setDefaultButton(button);
					} else {
						getRootPane().setDefaultButton(null);
					}
				}
			});
	}

	/**
	 * <p>Asynchronously set the button as enabled or disabled.</p>
	 *
	 * @param enabled <code>true</code> for the button to be enabled,
	 *                <code>false</code> to be disabled
	 */
	private void setButtonEnabled(final boolean enabled) {
		SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					button.setEnabled(enabled);
				}
			});
	}
}
