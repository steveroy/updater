## Introduction

Updater is generally inspired by the [Sparkle framework](http://sparkle.andymatuschak.org/) (Cocoa) by Andy Matuschak but is adapted to use Java concepts and to be cross-platform. The goal is to make the initial release of Updater work with zip archives only, for which support is built into the Java virtual machine on all platforms. Expansion of zip archives is easy to implement. Other file formats may come later.

## Checking For Updates

The premise is that a vendor publishes an [appcast](http://connectedflow.com/appcasting/) describing the updates available for their products. An appcast is similar to a podcast. It is an RSS feed whose items include enclosures, which is a feature of [RSS 2.0](http://cyber.law.harvard.edu/rss/rss.html). An example of such an appcast can be found at the link below.

View online with Firefox (or Camino), whereas Safari will ask you if you want to subscribe to the feed:
[Appcast Example](myapp/updates.xml)

The Updater API reads the feed and compares the version of the available updates with the current application version. A built-in user interface notifies the user when an update is found. The API offers many ways to customize this default behavior.

Version information is provided in the appcast via an extension to RSS under a specific namespace. This ensures that no name clashes ever occur. This is currently implemented as a ‘updater:version’ element required for each item in an appcast. Updater also supports the Sparkle mechanism, which is a ‘sparkle:version’ attribute added to the ‘enclosure’ element. If both are present, priority is given to the former.

## Documentation

The API documentation is [currently available here](www/docs/index.html).

Adding support for automatic check and update at the launch of an application is easy. All you have to do is instantiate an `Updater` object and call its `checkUpdates` method. If an update exists, you get a beautiful default UI giving users the choice of installing it. ([Mac](www/mac.png), [Windows](www/win.png), [Linux](www/lin.png))

    Updater updater = new Updater(
        "http://foo.bar.com/myapp/rss.xml", myApp.getName(), myApp.getVersion());
    updater.checkUpdates(false);

If you want to let the user perform the check from a menu item, keep the `Updater` instance around as a class variable and call it like this from the action listener of the menu item.

    public void actionListener(ActionEvent e) {
        updater.checkUpdates();
    }

If the user chose to ignore the update, you can get this information and store it in the application preferences. The API also has methods to monitor for updates continuously based on a given period of time (see `Updater.monitorUpdates()`). You can even suppress the default UI, which would be appropriate for a command line tool, or in order to replace it with a custom one via an `UpdateListener`.

## Status

At this point this is all implemented up to and including the download and expansion of the update archive. What is left to implement is the mechanism to actually replace the binaries of the application, which is the really tricky part of this project. There may be several platform-specific particularities to take into account. Hopefully one mechanism that works on all platforms can be achieved. Bugs in Java virtual machines will have to be taken into account. Therefore it will be important to initially define the platforms and versions of Java that are targeted for this project.

My first thought is that replacing binaries won’t work while the application is running, so we need to quit the application first. This implies that we can launch another process to take care of the upgrade before quitting.

There are two hurdles I’m foreseeing:

- Locating the application on disk
- Relaunching the application

We could locate the application disk through the use of user.dir, although it is not entirely reliable depending on how the application was launched. Is it enough for a first stab at it, documenting the conditions? Or should we resort to JNI or JNA? Or should we just bring up a dialog asking to locate the app if we can’t find it?

Once we know where it is it’s rather simple to compare the various files and upgrade them or delete deprecated ones.

Relaunching the application can be tricky as well. Each application has its own mechanism. How do we know which of the files is the launcher? Do we go with some basic assumptions (.app on Mac, .exe on Windows, but what about Linux and other Unixes)? Or do we put a hook in the API to let the calling code provide a callback that can perform this task?

Glossing over a lot of details, one strategy could be this:

1. Get the location of the application on disk.
2. Figure out how the application was launched.
3. Extract into tmp the class files of the installation tool.
4. Install a shutdown hook that will run the installation tool, passing the application location, the update location and the launch information.
5. Exit the application.
6. In the installation tool, compare the app and the archive and replace files as needed.
7. Relaunch the application.
8. Exit the installation tool.

## To Do

List of things that need to be improved or implemented in the Updater API:

- Find people willing to help on implementing the replacement on executable binaries
- Define the list of target platforms and Java VMs
- Figure out what the requirements and limitations are for replacing the executable binaries on various platforms
- Implement replacement of executable binaries
- Define the archive formats to eventually support
- Localize in more languages
