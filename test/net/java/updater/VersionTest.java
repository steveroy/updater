/*
 * Copyright 2009 Graham Wagener
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.java.updater;

import java.text.MessageFormat;
import java.text.ParseException;

import junit.framework.TestCase;

public class VersionTest extends TestCase {

	private static String getSegmentOrderingMessage(String segmentName,
		String ordering) {
		String pattern = "A Version should be {1} than another if its {0} is "
			+ "{1} than the other''s {0} when more significant segments are "
			+ "equal";
		return MessageFormat.format(pattern, segmentName, ordering);
	}

	private static String getSegmentPrecedenceMessage(String segmentName,
		String ordering) {
		String pattern = "Version segments of lesser significance than the {0} "
			+ "should be irrelevant if the {0} is {1} than the other";
		return MessageFormat.format(pattern, segmentName, ordering);
	}

	public void testConstructorForValidStringParameter() throws ParseException {
		assertEquals("1.1.1d1", new Version("1.1.1d1").toString());
	}

	public void testConstructorForEmptyStringParameter() {
		try {
			new Version("");
			fail("The empty string is not a proper format and should result in "
				+ "a ParseException");
		} catch (ParseException e) {}
	}

	public void testConstructorForStringParameterMissingMinorVersion() {
		try {
			new Version("1");
			fail("A string without \".MinorVersion\" is not properly formatted "
				+ "and should result in a ParseException");
		} catch (ParseException e) {}
	}

	public void testConstructorForStringParameterMissingPrereleaseVersion() {
		try {
			new Version("1.1d");
			fail("A string with \"StageCode\", but without \"PrereleaseVersion"
				+ "\" is not properly formatted and should result in a "
				+ "ParseException");
		} catch (ParseException e) {}
	}

	public void testConstructorForStringParameterMissingStageCode() {
		try {
			new Version("1.1.1.1");
			fail("A string with \".PrereleaseVersion\" instead of \"StageCode "
				+ "PrereleaseVersion\" (without a space) is not properly "
				+ "formatted and should result in a ParseException");
		} catch (ParseException e) {}
	}

	public void testCompareToEqualParameter() throws ParseException {
		Version test = new Version("1.0");
		assertTrue("The comparison result if this Version is equal to the "
			+ "specified object should be 0", test.compareTo(test) == 0);
	}

	public void testCompareToForGreaterBugVersionOrdering()
		throws ParseException {
		String msg = getSegmentOrderingMessage("bug version", "less");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("2.2.3a2")) < 0);
	}

	public void testCompareToForGreaterBugVersionPrecedence()
		throws ParseException {
		String msg = getSegmentPrecedenceMessage("bug version", "less");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("2.2.3d1")) < 0);
	}

	public void testCompareToForGreaterMajorVersionOrdering()
		throws ParseException {
		String msg = getSegmentOrderingMessage("major version", "less");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("3.2.2a2")) < 0);
	}

	public void testCompareToForGreaterMajorVersionPrecedence()
		throws ParseException {
		String msg = getSegmentPrecedenceMessage("major version", "less");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("3.1.1d1")) < 0);
	}

	public void testCompareToForGreaterMinorVersionOrdering()
		throws ParseException {
		String msg = getSegmentOrderingMessage("minor version", "less");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("2.3.2a2")) < 0);
	}

	public void testCompareToForGreaterMinorVersionPrecedence()
		throws ParseException {
		String msg = getSegmentPrecedenceMessage("minor version", "less");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("2.3.1d1")) < 0);
	}

	public void testCompareToForGreaterPrereleaseVersionOrdering()
		throws ParseException {
		String msg = getSegmentOrderingMessage("prerelease version", "less");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("2.2.2a3")) < 0);
	}

	public void testCompareToForGreaterStageOrdering() throws ParseException {
		String msg = getSegmentOrderingMessage("stage", "less");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("2.2.2b2")) < 0);
	}

	public void testCompareToForGreaterStagePrecedence() throws ParseException {
		String msg = getSegmentPrecedenceMessage("stage", "less");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("2.2.2b1")) < 0);
	}

	public void testCompareToForLesserBugVersionOrdering()
		throws ParseException {
		String msg = getSegmentOrderingMessage("bug version", "greater");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("2.2.1a2")) > 0);
	}

	public void testCompareToForLesserBugVersionPrecedence()
		throws ParseException {
		String msg = getSegmentPrecedenceMessage("bug version", "greater");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("2.2.1b3")) > 0);
	}

	public void testCompareToForLesserMajorVersionOrdering()
		throws ParseException {
		String msg = getSegmentOrderingMessage("major version", "greater");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("1.2.2a2")) > 0);
	}

	public void testCompareToForLesserMajorVersionPrecedence()
		throws ParseException {
		String msg = getSegmentPrecedenceMessage("major version", "greater");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("1.3.3b3")) > 0);
	}

	public void testCompareToForLesserMinorVersionOrdering()
		throws ParseException {
		String msg = getSegmentOrderingMessage("minor version", "greater");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("2.1.2a2")) > 0);
	}

	public void testCompareToForLesserMinorVersionPrecedence()
		throws ParseException {
		String msg = getSegmentPrecedenceMessage("minor version", "greater");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("2.1.3b3")) > 0);
	}

	public void testCompareToForLesserPrereleaseVersionOrdering()
		throws ParseException {
		String msg = getSegmentOrderingMessage("prerelease version", "greater");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("2.2.2a1")) > 0);
	}

	public void testCompareToForLesserStageOrdering() throws ParseException {
		String msg = getSegmentOrderingMessage("stage", "greater");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("2.2.2d2")) > 0);
	}

	public void testCompareToForLesserStagePrecedence() throws ParseException {
		String msg = getSegmentPrecedenceMessage("stage", "greater");
		Version test = new Version("2.2.2a2");
		assertTrue(msg, test.compareTo(new Version("2.2.2d3")) > 0);
	}

	public void testCompareToGreaterParameter() throws ParseException {
		Version test = new Version("1.0");
		assertTrue("The comparison result if this Version is less than the "
			+ "specified object should be a negative integer",
			test.compareTo(new Version("2.0")) < 0);
	}

	public void testCompareToLesserParameter() throws ParseException {
		Version test = new Version("1.0");
		assertTrue("The comparison result if this Version is greater than the "
			+ "specified object should be a positive integer",
			test.compareTo(new Version("0.0")) > 0);
	}

	public void testCompareToNonVersionParameter() throws ParseException {
		Version test = new Version("1.0");
		try {
			test.compareTo(this);
			fail("Comparing an object of class other than Version should result"
				+ " in a ClassCasteException");
		} catch (ClassCastException e) {}
	}

	public void testCompareToNullParameter() throws ParseException {
		Version test = new Version("1.0");
		assertTrue("Any Version should be greater than null",
			test.compareTo(null) > 0);
	}

	public void testEqualsForEqualParameter() throws ParseException {
		Version test = new Version("1.2.3d4");
		assertTrue("A Version should equal another Version with all the same "
			+ "segments", test.equals(new Version("1.2.3d4")));
	}

	public void testEqualsForNonVersionParameter() throws ParseException {
		Version test = new Version("1.2.3d4");
		assertFalse("A Version should not equal an object of another class",
			test.equals(this));
	}

	public void testEqualsForNullParameter() throws ParseException {
		Version test = new Version("1.2.3d4");
		assertFalse(test.equals(null));
	}

	public void testEqualsForSameObjectParameter() throws ParseException {
		Version test = new Version("1.2.3d4");
		assertTrue("A Version equals itself", test.equals(test));
	}

	public void testEqualsForUnequalBugVersion() throws ParseException {
		Version test = new Version("1.2.3d4");
		assertFalse("A Version should not equal another Version with a "
			+ "different bug version", test.equals(new Version("1.2.0d4")));
	}

	public void testEqualsForUnequalMajorVersion() throws ParseException {
		Version test = new Version("1.2.3d4");
		assertFalse("A Version should not equal another Version with a "
			+ "different major version", test.equals(new Version("0.2.3d4")));
	}

	public void testEqualsForUnequalMinorVersion() throws ParseException {
		Version test = new Version("1.2.3d4");
		assertFalse("A Version should not equal another Version with a "
			+ "different minor version", test.equals(new Version("1.0.3d4")));
	}

	public void testEqualsForUnequalPrereleaseVersion() throws ParseException {
		Version test = new Version("1.2.3d4");
		assertFalse("A Version should not equal another Version with a "
			+ "different prerelease version",
			test.equals(new Version("1.2.3d0")));
	}

	public void testEqualsForUnequalStageCode() throws ParseException {
		Version test = new Version("1.2.3d4");
		assertFalse("A Version should not equal another Version with a "
			+ "different stage code", test.equals(new Version("1.2.3a4")));
	}

	public void testHashCode() throws ParseException {
		assertEquals("Equal Versions should have the same hash code",
			new Version("1.0").hashCode(), new Version("1.0").hashCode());
	}

	public void testIsGreaterThanForLesserVersionParameter()
		throws ParseException {
		Version test = new Version("1.0");
		assertTrue(test.isGreaterThan(new Version("0.1")));
	}

	public void testIsGreaterThanForGreaterVersionParameter()
		throws ParseException {
		Version test = new Version("1.0");
		assertFalse(test.isGreaterThan(new Version("1.1")));
	}

	public void testIsGreaterThanForEqualVersionParameter()
		throws ParseException {
		Version test = new Version("1.0");
		assertFalse(test.isGreaterThan(test));
	}

	public void testToStringForBugVersionNotZero() throws ParseException {
		String stringRep = "1.1.1";
		assertEquals("Bug version should be included if not 0", stringRep,
			new Version(stringRep).toString());
	}

	public void testToStringForBugVersionZero() throws ParseException {
		assertEquals("Bug version should not be included if 0", "0.0",
			new Version("0.0.0").toString());
	}

	public void testToStringForMajorMinorVersionsZero() throws ParseException {
		String stringRep = "0.0";
		assertEquals("Major and minor versions should always be included",
			stringRep, new Version(stringRep).toString());
	}

	public void testToStringForPrerelease() throws ParseException {
		String stringRep = "1.1a0";
		assertEquals("Stage code and prerelease version should be included if "
			+ "stage is not FINAL", stringRep,
			new Version(stringRep).toString());
	}
}
