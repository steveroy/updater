package net.java.updater;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.ParseException;
import java.util.prefs.Preferences;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.Timer;
import javax.swing.UIManager;

public final class Test {

	private static final String IGNORED_UPDATE_PREF = "Test.ignoredUpdate";

	static {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception ex) {
			// Oh well, let's just use whatever L&F we get
		}
		
		System.setProperty("apple.laf.useScreenMenuBar", "true");
	}
	
	private static Updater updater;

	private static JMenuItem startMonitoringItem;
	private static JMenuItem stopMonitoringItem;

	/**
	 * Private constructor prevents instanciation of utility class.
	 */
	private Test() {
		// Default constructor
	}

	public static void main(String[] args) throws ParseException {
		JFrame f = new JFrame("Test");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(300, 300);
		f.addWindowListener(new WindowAdapter() {
				public void windowOpened(WindowEvent e) {
					Timer t = new Timer(2000, new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								updater.checkUpdates(false);
							}
						});
					t.setRepeats(false);
					t.start();
				}
			});

		JMenuBar mb = new JMenuBar();
		f.setJMenuBar(mb);
		
		JMenu m = new JMenu("File");
		mb.add(m);
		
		JMenuItem mi = new JMenuItem("Check for Update...");
		mi.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					updater.checkUpdates();
				}
			});
		m.add(mi);
		
		m.addSeparator();

		startMonitoringItem = new JMenuItem("Monitor Updates");
		startMonitoringItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					updater.monitorUpdates(10); // Every 10 seconds
				}
			});
		m.add(startMonitoringItem);

		stopMonitoringItem = new JMenuItem("Stop Monitoring");
		stopMonitoringItem.setEnabled(false);
		stopMonitoringItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					updater.stopMonitoring();
				}
			});
		m.add(stopMonitoringItem);

		f.setVisible(true);
		
		updater = new Updater(
//			"http://opensword.org/pixen/PixenChangelog.xml",
//			"http://update.macrabbit.com/cssedit/2.5.1.xml",
//			"http://www.codingmonkeys.de/subethaedit/appcast.rss",
//			"http://1passwd.com/versions/releases",
			"https://aptapps.ca/updater/myapp/updates.xml",
			"My App", new Version("1.0"));
		updater.addUpdateListener(new UpdateListener() {
				public void checkFailed(UpdateEvent e) {
					System.out.println(e);
				}
				
				public void updateNotFound(UpdateEvent e) {
					System.out.println(e);
				}
				
				public void upToDate(UpdateEvent e) {
					System.out.println(e);
				}
				
				public void updateIgnored(UpdateEvent e) {
					System.out.println(e);
				}
				
				public void updateAvailable(UpdateEvent e) {
					System.out.println(e);
				}
				
				public void applicationRestarting(UpdateEvent e) {
					System.out.println(e);
				}
			});
		updater.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent e) {
				String prop = e.getPropertyName();
				System.out.println(e.getClass().getName()
						+ " [" + prop + "=" + e.getNewValue() + "]");
				if (prop == Updater.MONITORING_PROPERTY) {
					// Update the state of the menu items
					boolean monitoring = (e.getNewValue() == Boolean.TRUE);
					startMonitoringItem.setEnabled(!monitoring);
					stopMonitoringItem.setEnabled(monitoring);
				} else if (prop == Updater.IGNORED_VERSION_PROPERTY) {
					// Persist the ignored version in the preferences
					Preferences prefs = Preferences.userRoot();
					Version ignored = (Version) e.getNewValue();
					if (ignored != null) {
						prefs.put(IGNORED_UPDATE_PREF, ignored.toString());
					} else {
						prefs.remove(IGNORED_UPDATE_PREF);
					}
				}
			}
		});

		// Restore the ignored version
		Preferences prefs = Preferences.userRoot();
		String ignored = prefs.get(IGNORED_UPDATE_PREF, null);
		if (ignored != null) {
			updater.setIgnoredVersion(new Version(ignored));
		}
	}
}
