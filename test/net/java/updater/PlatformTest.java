package net.java.updater;

import junit.framework.TestCase;

public class PlatformTest extends TestCase {

	public void testConstructor() {
		try {
			new Platform("");
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	public void testEqualsForEqualParameter() {
		Platform test = new Platform("Windows");
		assertTrue("A Platform should equal another Platform that responds the "
			+ "same to all isPlatformX() calls",
			test.equals(new Platform("Windows")));
	}

	public void testEqualsForNonStageParameter() {
		assertFalse("A Platform should not equal an object of another class",
			Platform.getCurrentPlatform().equals(this));
	}

	public void testEqualsForNullObjectParameter() {
		assertFalse(Platform.getCurrentPlatform().equals(null));
	}

	public void testEqualsForSameObjectParameter() {
		assertTrue("A Platform equals itself",
			Platform.getCurrentPlatform().equals(
				Platform.getCurrentPlatform()));
	}

	public void testEqualsForUnequalParameter() {
		Platform test = new Platform("Mac OS X");
		assertFalse("A Platform should not equal another Platform that does not"
			+ " respond the same to all isPlatformX() calls",
			test.equals(new Platform("Windows")));
	}

	public void testGetCurrentPlatform() {
		assertEquals("getCurrentPlatform should return the same value as "
			+ "constructing a Platform with system property \"os.name\"",
			new Platform(System.getProperty("os.name")),
			Platform.getCurrentPlatform());
	}

	public void testHashCode() {
		assertEquals("Equal Platforms should have the same hash code",
			Platform.getCurrentPlatform().hashCode(),
			Platform.getCurrentPlatform().hashCode());
	}

	public void testIsMacForMac() {
		assertTrue("A Mac platform should return true to isMac()",
			new Platform("Mac OS X").isMac());
	}

	public void testIsMacForNotMac() {
		assertFalse("A non-Mac platform should not return true to isMac()",
			new Platform("").isMac());
	}

	public void testIsUnixForNotUnix() {
		assertFalse("A non-Unix platform should not return true to isUnix()",
			new Platform("").isUnix());
	}

	public void testIsUnixForUnix() {
		assertTrue("A Unix system should return true to isUnix()",
			new Platform("Unix").isUnix());
	}

	public void testIsWindowsForNotWindows() {
		assertFalse("A non-Windows platform should not return true to "
			+ "isWindows()", new Platform("").isWindows());
	}

	public void testIsWindowsForWindows() {
		assertTrue("A Windows platform should return true to isWindows()",
			new Platform("Windows").isWindows());
		assertTrue("Any Windows platform should return true to isWindows()",
			new Platform("Windows 98").isWindows());
	}
}
