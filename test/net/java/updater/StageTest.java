/*
 * Copyright 2009 Graham Wagener
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.java.updater;

import junit.framework.TestCase;

public class StageTest extends TestCase {

	public void testConstantAlpha() {
		assertEquals("Stage.ALPHA should is represented by \"a\" so should be "
			+ "equal to a Stage constructed with \"a\"", new Stage("a"),
			Stage.ALPHA);
	}

	public void testConstantAlphaOrder() {
		assertTrue("Stage.ALPHA should be greater in order than "
			+ "Stage.DEVELOPMENT",
			Stage.ALPHA.compareTo(Stage.DEVELOPMENT) > 0);
	}

	public void testConstantBeta() {
		assertEquals("Stage.BETA should is represented by \"b\" so should be "
			+ "equal to a Stage constructed with \"b\"", new Stage("b"),
			Stage.BETA);
	}

	public void testConstantBetaOrder() {
		assertTrue("Stage.BETA should be greater in order than Stage.ALPHA",
			Stage.BETA.compareTo(Stage.ALPHA) > 0);
	}

	public void testConstantDevelopment() {
		assertEquals("Stage.DEVELOPMENT should is represented by \"d\" so "
			+ "should be equal to a Stage constructed with \"d\"",
			new Stage("d"), Stage.DEVELOPMENT);
	}

	public void testConstantReleaseCandidate() {
		assertEquals("Stage.RELEASE_CANDIDATE should is represented by \"rc\" "
			+ "so should be equal to a Stage constructed with \"rc\"",
			new Stage("rc"), Stage.RELEASE_CANDIDATE);
	}

	public void testConstantReleaseCandidateOrder() {
		assertTrue("Stage.RELEASE_CANDIDATE should be greater in order than "
			+ "Stage.BETA", Stage.RELEASE_CANDIDATE.compareTo(Stage.BETA) > 0);
	}

	public void testConstantFinalOrder() {
		assertTrue("Stage.FINAL should be greater in order than "
			+ "Stage.RELEASE_CANDIDATE",
			Stage.FINAL.compareTo(Stage.RELEASE_CANDIDATE) > 0);
	}

	public void testConstructorForEmptyStringParameter() {
		try {
			new Stage("");
			fail("The empty string is an invalid parameter and should result in"
				+ " an IllegalArgumentException");
		} catch (IllegalArgumentException e) {}
	}

	public void testConstructorForInvalidParameter() {
		try {
			new Stage("x");
			fail("An invalid parameter should result in an "
				+ "IllegalArgumentException");
		} catch (IllegalArgumentException e) {}
	}

	public void testConstructorForNullParameter() {
		try {
			new Stage(null);
			fail("null is an invalid parameter and should result in an "
				+ "IllegalSrgumentException");
		} catch (IllegalArgumentException e) {}
	}

	public void testConstructorForValidParameter() {
		new Stage("a");
	}

	public void testCompareToEqualObjectParameter() {
		assertTrue("The comparison result if this Stage is equal to the "
			+ "specified Object should be 0",
			Stage.ALPHA.compareTo(Stage.ALPHA) == 0);
	}

	public void testCompareToGreaterObjectParameter() {
		assertTrue("The comparison result if this Stage is less than the "
			+ "specified Object should be a negative integer",
			Stage.DEVELOPMENT.compareTo(Stage.FINAL) < 0);
	}

	public void testCompareToLesserObjectParameter() {
		assertTrue("The comparison result if this Stage is greater than the"
			+ "specified Object should be a positive integer",
			Stage.FINAL.compareTo(Stage.DEVELOPMENT) > 0);
	}

	public void testCompareToNonStageParameter() {
		try {
			Stage.ALPHA.compareTo(this);
			fail("Comparing an object of class other than Stage should "
				+ "result in a ClassCasteException");
		} catch (ClassCastException e) {}
	}

	public void testCompareToNullObjectParamter() {
		assertTrue("Any Stage should be greater than null",
			Stage.ALPHA.compareTo(null) > 0);
	}

	public void testEqualsForEqualParameter() {
		Stage test = new Stage("a");
		assertTrue("A Stage should equal another Stage with the same order",
			test.equals(new Stage("a")));
	}

	public void testEqualsForNonStageParameter() {
		assertFalse("A Stage should not equal an object of another class",
			Stage.ALPHA.equals(this));
	}

	public void testEqualsForNullObjectParameter() {
		assertFalse(Stage.ALPHA.equals(null));
	}

	public void testEqualsForSameObjectParameter() {
		assertTrue("A Stage equals itself", Stage.ALPHA.equals(Stage.ALPHA));
	}

	public void testEqualsForUnequalParameter() {
		assertFalse("A Stage should not equal another Stage with a different "
			+ "order", Stage.ALPHA.equals(Stage.BETA));
	}

	public void testHashCode() {
		assertEquals("Equal Stages should have the same hash code",
			Stage.ALPHA.hashCode(), Stage.ALPHA.hashCode());
	}

	public void testToString() {
		assertEquals("toString should return the same representation as the "
			+ "Stage was created with", "a", Stage.ALPHA.toString());
	}
}
